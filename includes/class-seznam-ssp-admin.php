<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_Admin
{

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Constructor
     *
     * @since     1.0.0
     */

    public function __construct()
    {

        // Add styles
        if (SSSP_Admin::check_admin()) {
            add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_styles']);
            add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_scripts']);
        }

    }


    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Register and enqueue admin-specific style sheet.
     */
    public function enqueue_admin_styles()
    {
        wp_enqueue_style(SEZNAM_SSP_SLUG . '-style', SEZNAM_SSP_URL . 'assets/css/admin.min.css', [], SEZNAM_SSP_VERSION);

        //Enqueue the jQuery UI theme css file from google:
        $wp_scripts = wp_scripts();

        wp_enqueue_style(
            'jquery-ui-theme-smoothness', //select ui theme: base...
            sprintf(
                'https://ajax.googleapis.com/ajax/libs/jqueryui/%s/themes/smoothness/jquery-ui.css',
                $wp_scripts->registered['jquery-ui-core']->ver
            )
        );
    }

    /**
     *
     */
    public function enqueue_admin_scripts()
    {
        wp_enqueue_script(SEZNAM_SSP_SLUG . '-admin-js', SEZNAM_SSP_URL . 'assets/js/admin.min.js', ['jquery'], SEZNAM_SSP_VERSION);

        if (isset($_GET['page']) && $_GET['page'] == 'seznam-sssp-admin') {
            $cm_settings['codeEditor'] = wp_enqueue_code_editor(['type' => 'text/css']);
            wp_localize_script('jquery', 'cm_settings', $cm_settings);
            wp_enqueue_script('wp-theme-plugin-editor');
            wp_enqueue_style('wp-codemirror');
        }

        wp_enqueue_script('jquery-ui-core', false, ['jquery']);
        wp_enqueue_script("jquery-ui-tabs");

    }

    /**
     * @return bool
     */
    public static function check_admin()
    {
        if (isset($_GET['post_type']) && $_GET['post_type'] == SEZNAM_SSP_POST_TYPE_SLUG) {
            return true;
        }
        if (isset($_GET['post']) && get_post_type($_GET['post']) == SEZNAM_SSP_POST_TYPE_SLUG) {
            return true;
        }
        if (isset($_GET['page']) && $_GET['page'] == 'seznam-sssp-admin') {
            return true;
        }
        if (isset($_POST['sssp-zone-id-message'])) {
            return true;
        }
        if (isset($_POST['sssp-mobile-breakpoint'])) {
            return true;
        }
        if (isset($_POST['sssp-admin-save'])) {
            return true;
        }

        return false;
    }


}