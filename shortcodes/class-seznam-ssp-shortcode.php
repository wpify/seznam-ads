<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_Shortcode_SSP
{
    protected static $instance = NULL;

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    public function __construct()
    {
        //Create Image Gallery Custom Post
        add_shortcode('seznam-ssp', [$this, 'shortcode']);

    }

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (NULL == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param $atts
     * @param null $content
     * @return string
     */
    public function shortcode($atts, $content = NULL)
    {
        if (isset($atts['id'])) {
            $post_id = $atts['id'];
        } else {
            return null;
        }

        return do_shortcode('[seznam-ads id="' . $post_id . '"]');

    }


}

