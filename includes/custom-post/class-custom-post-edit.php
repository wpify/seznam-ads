<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_CustomPostEdit
{

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    protected static $instance = NULL;

    /**
     * Constructor
     *
     * @since     1.0.0
     */

    public function __construct()
    {
        add_action('do_meta_boxes', [$this, 'add_meta_boxes']); //Add Meta boxes
        add_action('save_post_' . SEZNAM_SSP_POST_TYPE_SLUG, [$this, 'item_save']); //Save post
    }

    /**
     * @return SSSP_CustomPostEdit|null
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (NULL == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Remove and add metaboxes
     */
    public function add_meta_boxes()
    {
        add_meta_box('sssp-main-settings', // meta box ID
            esc_attr__('Main settings', SEZNAM_SSP_SLUG), // meta box title
            [$this, 'main_settings'], // callback function that prints the meta box HTML
            SEZNAM_SSP_POST_TYPE_SLUG, // post type where to add it
            'advanced', // context
            'high') // position
        ;
        add_meta_box('sssp-publish-settings', // meta box ID
            esc_attr__('Publishing settings', SEZNAM_SSP_SLUG), // meta box title
            [$this, 'publish_settings'], // callback function that prints the meta box HTML
            SEZNAM_SSP_POST_TYPE_SLUG, // post type where to add it
            'side', // context
            'default') // position
        ;
        add_meta_box('sssp-allowed-post-types', // meta box ID
            esc_attr__('Allowed post types', SEZNAM_SSP_SLUG), // meta box title
            [$this, 'allowed_post_types'], // callback function that prints the meta box HTML
            SEZNAM_SSP_POST_TYPE_SLUG, // post type where to add it
            'side', // context
            'default') // position
        ;
        add_meta_box('sssp-notes-block', // meta box ID
            esc_attr__('Notes', SEZNAM_SSP_SLUG), // meta box title
            [$this, 'notes_block'], // callback function that prints the meta box HTML
            SEZNAM_SSP_POST_TYPE_SLUG, // post type where to add it
            'side', // context
            'default') // position
        ;
    }

    /**
     * @param $post
     */
    public function main_settings($post)
    {
        $post_id = $post->ID;

        $html = '<div class="sssp-post-table sssp-block">';

        $html .= '<div id="sssp-data-holder"
        data-error="' . esc_attr__('Zone ID is not valid or ad is not active', SEZNAM_SSP_SLUG) . '"
        data-hidden-message="' . esc_attr__('Zone ID is verified', SEZNAM_SSP_SLUG) . '"></div>';

        $html .= '<div class="sssp-post-table-left">';
        $html .= $this->ad_settings($post, $post_id);
        $html .= $this->ad_insert($post, $post_id);
        $html .= $this->ad_preview($post, $post_id);
        $html .= '</div>';
        $html .= '<div class="sssp-post-table-right" id="sssp-post-table-right">';
        $html .= $this->ad_help($post, $post_id);
        $html .= '</div>';

        $html .= '<div class="sssp-clear" id="sssp-bottom-div"></div>';

        $html .= '</div>'; // Let's get started

        echo $html;
    }

    /**
     * @param $post
     */
    public function publish_settings($post)
    {
        $post_id = $post->ID;

        $html = '<div class="sssp-post-table sssp-block sssp-form">';

        $html .= '<div class="sssp-post-table-sidebar">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Publishing settings', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'radio',
            'name' => 'sssp-ad-active',
            'label' => esc_attr__('Ad status', SEZNAM_SSP_SLUG),
            'values' => [
                'active' => esc_attr__('Active', SEZNAM_SSP_SLUG),
                'notactive' => esc_attr__('Not active', SEZNAM_SSP_SLUG),
                'wait' => esc_attr__('Waiting for approval', SEZNAM_SSP_SLUG),
                'indev' => esc_attr__('Development mode', SEZNAM_SSP_SLUG) . '*',
            ],
            'option' => get_post_meta($post_id, 'sssp-ad-active', true),
            'default' => 'indev',
            'required' => 'required',
            'status' => '',
        ]);
        $html .= '<p>*' . esc_attr__('Development mode allows you to safely check your ad before publishing. The ad will only be visible to users authorized to manage the SSP Seznam plugin. If you want to use development mode, be sure to disable caching.', SEZNAM_SSP_SLUG) . '</p>';

        $html .= '</div>'; //sssp-post-table-sidebar

        $html .= '<div class="sssp-clear"></div>';

        $html .= '</div>'; // sssp-post-table sssp-block

        echo $html;
    }

    /**
     * @param $post
     * @param $post_id
     * @return string
     */
    private function ad_settings($post, $post_id)
    {
        $html = '<div class="sssp-settings-box sssp-form">';

        $html .= '<form id="sssp-ad-settings" method="post">';

        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Ad basic settings', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'hidden',
            'name' => 'sssp-zone-id-message',
            'value' => get_post_meta($post_id, 'sssp-zone-id-message', true),
            'default' => esc_attr__('Zone ID is verified', SEZNAM_SSP_SLUG),
        ]);

        $html .= '<div class="sssp-width-50">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'text',
            'name' => 'sssp-zone-id',
            'label' => esc_attr__('Zone ID', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-zone-id', true),
            'required' => 'required',
        ]);
        $html .= '</div>';
        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'number',
            'name' => 'sssp-zone-width',
            'label' => esc_attr__('Width', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-zone-width', true),
            'min' => '1',
            'max' => '1000000',
            'step' => '1',
            'required' => 'required',
            'status' => '',
            'placeholder' => esc_attr__('Width', SEZNAM_SSP_SLUG) . '...',
            'default' => '',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'number',
            'name' => 'sssp-zone-height',
            'label' => esc_attr__('Height', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-zone-height', true),
            'min' => '0',
            'max' => '1000000',
            'step' => '1',
            'required' => 'required',
            'status' => '',
            'placeholder' => esc_attr__('Height', SEZNAM_SSP_SLUG) . '...',
            'default' => '',
        ]);
        $html .= '</div>';


        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Ad placement', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-zone-insert',
            'label' => esc_attr__('Insert', SEZNAM_SSP_SLUG),
            'options' => [
                'manual' => esc_attr__('Manually', SEZNAM_SSP_SLUG),
                'automatic' => esc_attr__('Automatically', SEZNAM_SSP_SLUG),
            ],
            'value' => get_post_meta($post_id, 'sssp-zone-insert', true),
            'required' => 'required',
            'status' => '',
            'default' => '',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-25">';
        $standard_positions = $this->get_standard_ad_positions();
        $options = [];
        foreach ($standard_positions as $s_key => $s_value) {
            $options[$s_key] = $s_value['name'];
        }
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-zone-position',
            'label' => esc_attr__('Position', SEZNAM_SSP_SLUG),
            'options' => $options,
            'value' => get_post_meta($post_id, 'sssp-zone-position', true),
            'required' => 'required',
            'status' => '',
            'default' => '',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'number',
            'name' => 'sssp-zone-inarticle-placement',
            'label' => esc_attr__('After paragraph No.', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-zone-inarticle-placement', true),
            'min' => '0',
            'max' => '1000000',
            'step' => '1',
            'required' => 'required',
            'status' => 'disabled',
            'placeholder' => '',
            'default' => '1',
        ]);
        $html .= '</div>';
        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-zone-inarticle-placement-repeat',
            'label' => esc_attr__('Repeat after 4th paragraph', SEZNAM_SSP_SLUG),
            'options' => [
                'yes' => esc_attr__('Yes', SEZNAM_SSP_SLUG),
                'no' => esc_attr__('No', SEZNAM_SSP_SLUG),
            ],
            'value' => get_post_meta($post_id, 'sssp-zone-inarticle-placement-repeat', true),
            'required' => 'required',
            'status' => 'disabled',
            'default' => 'no',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-100">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Ad styling', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '</div>';


        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-ad-alignment',
            'label' => esc_attr__('Alignment', SEZNAM_SSP_SLUG),
            'options' => [
                'center' => esc_attr__('Center', SEZNAM_SSP_SLUG),
                'left' => esc_attr__('Left', SEZNAM_SSP_SLUG),
                'right' => esc_attr__('Right', SEZNAM_SSP_SLUG),
                'none' => esc_attr__('No alignment', SEZNAM_SSP_SLUG),
            ],
            'value' => get_post_meta($post_id, 'sssp-ad-alignment', true),
            'required' => '',
            'status' => '',
            'default' => 'center',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'number',
            'name' => 'sssp-ad-margin-top',
            'label' => esc_attr__('Margin top (value in pixels)', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-ad-margin-top', true),
            'min' => '-1000000',
            'max' => '100',
            'step' => '1',
            'required' => '',
            'status' => '',
            'placeholder' => '',
            'default' => '0',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'number',
            'name' => 'sssp-ad-margin-bottom',
            'label' => esc_attr__('Margin bottom (value in pixels)', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-ad-margin-bottom', true),
            'min' => '-1000000',
            'max' => '100',
            'step' => '1',
            'required' => '',
            'status' => '',
            'placeholder' => '',
            'default' => '0',
        ]);
        $html .= '</div>';


        $html .= '<div class="sssp-width-25">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'text',
            'name' => 'sssp-ad-custom-css',
            'label' => esc_attr__('Add your own class', SEZNAM_SSP_SLUG),
            'value' => get_post_meta($post_id, 'sssp-ad-custom-css', true),
            'required' => '',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-50">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Ad type', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '</div>';
        $html .= '<div class="sssp-width-50 sssp-desktop">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Special settings', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '</div>';

        $html .= '<div class="sssp-width-50">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-ad-type',
            'label' => esc_attr__('Ad type', SEZNAM_SSP_SLUG),
            'options' => [
                'mobile' => esc_attr__('Mobile devices', SEZNAM_SSP_SLUG),
                'desktop' => esc_attr__('Desktop', SEZNAM_SSP_SLUG),
            ],
            'value' => get_post_meta($post_id, 'sssp-ad-type', true),
            'required' => '',
            'status' => '',
            'default' => 'desktop',
        ]);
        $html .= '</div>';


        $html .= '<div class="sssp-width-50 sssp-mobile">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Special settings', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '</div>';

        $html .= '<div class="sssp-width-50" style="vertical-align: top; margin-top:-2px;">';

        $sssp_option_value = get_post_meta($post_id, 'sssp-ad-newsfeed', true);
        $sssp_option_value = (empty ($sssp_option_value)) ? ('0') : ($sssp_option_value);

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'radio',
            'name' => 'sssp-ad-newsfeed',
            'label' => esc_attr__('Layout type', SEZNAM_SSP_SLUG),
            'values' => [
                '0' => esc_attr__('Regular (default)', SEZNAM_SSP_SLUG).' <big>*</big>',
                '1' => esc_attr__('Show ad only to visitors from Newsfeed', SEZNAM_SSP_SLUG).' <big>*</big>',
            ],
            'option' => $sssp_option_value,
            'default' => '0',
            'required' => 'required',
            'status' => '',
        ]);

        $html .= '<br><small style="font-size:0.9em">* ' . $this->help_special_settings_small() . '</small>';

        $html .= '</div>';


        $html .= '</form>';

        $html .= '</div>'; //sssp-settings-box sssp-form
        return $html;
    }

    private function ad_insert($post, $post_id)
    {

        $html = '<div class="sssp-settings-box" id="sssp-insert-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('How to insert an ad into the content in manual mode?', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '<div id="tabs">';
        $html .= '<ul>';
        $html .= '<li><a href="#tabs-1">' . esc_attr__('Shortcode', SEZNAM_SSP_SLUG) . '</a></li>';
        $html .= '<li><a href="#tabs-2">' . esc_attr__('PHP', SEZNAM_SSP_SLUG) . '</a></li>';
        $html .= '<li><a href="#tabs-3">' . esc_attr__('HTML', SEZNAM_SSP_SLUG) . '</a></li>';
        $html .= '</ul>';
        $html .= '<div id="tabs-1">';
        $html .= '<p>' . esc_attr__('Shortcode you can use directly in the articles and most types of posts. Almost all page builders, WYSIWYG editors, and widgets also support shortcodes. If your widget, editor, or page builder does not support shortcode use HTML code instead!', SEZNAM_SSP_SLUG) . '</p>';
        $html .= '<textarea class="sssp-textarea-preview sssp-textarea-preview-shortcode">[' . SEZNAM_SSP_SLUG . ' id="' . $post_id . '"]</textarea>';
        $html .= '<div class="sssp-width-100 sssp-form">';
        $html .= '<div class="sssp-form-button-box-2" data-target="shortcode">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'submit',
            'name' => 'sssp-copy-code',
            'label' => esc_attr__('Copy code', SEZNAM_SSP_SLUG),
        ]);
        $html .= '</div>';// sssp-form-button-box
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div id="tabs-2">';
        $html .= '<p>' . esc_attr__('This type of insertion should be managed by advanced users only!', SEZNAM_SSP_SLUG) . '</p>';
        $html .= '<textarea class="sssp-textarea-preview sssp-textarea-preview-php">&#x3C;?php echo do_shortcode(&#x27;[' . SEZNAM_SSP_SLUG . ' id="' . $post_id . '"]&#x27;); ?&#x3E;</textarea>';
        $html .= '<div class="sssp-width-100 sssp-form">';
        $html .= '<div class="sssp-form-button-box-2" data-target="php">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'submit',
            'name' => 'sssp-copy-code',
            'label' => esc_attr__('Copy code', SEZNAM_SSP_SLUG),
        ]);
        $html .= '</div>';// sssp-form-button-box
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div id="tabs-3">';
        $html .= '<p>' . esc_attr__('This type of insertion should be used only as of the last option. Shortcode and PHP method is better because all changes you will make will be automatically applied to the content. But not with the HTML insertion. After you do a change in the ad settings, you must replace the old HTML code with the new one.', SEZNAM_SSP_SLUG) . '</p>';
        $html .= $this->render_html_ad($post_id);
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<script>jQuery(document).ready(function($) {';
        $html .= '$(\'#tabs\').tabs();})';
        $html .= '</script>';

        $html .= '</div>';
        return $html;

    }

    /**
     * @param $post
     * @param $post_id
     * @return string
     */
    private function ad_preview($post, $post_id)
    {
        add_thickbox();
        $html = '<div class="sssp-settings-box" id="sssp-preview-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Ad position preview for desktop', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '<div class="sssp-image-preview-box">';
        $html .= $this->get_preview_images_desktop($post_id, $this->get_standard_ad_positions());
        $html .= $this->get_preview_text_desktop();
        $html .= '</div>'; //sssp-image-preview-box
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Ad position preview for mobile devices', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '<div class="sssp-image-preview-box">';
        $html .= $this->get_preview_images_mobile();
        $html .= $this->get_preview_text_mobile();
        $html .= '</div>'; //sssp-image-preview-box
        $html .= '</div>';
        return $html;
    }

    /**
     * @param $post
     * @param $post_id
     * @return string
     */
    private function ad_help($post, $post_id)
    {

        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Help', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= $this->insert_manual_automatic_help($post, $post_id);
        $html .= '</div>';

        return $html;
    }

    /**
     * @param $post
     * @param $post_id
     * @return string
     */
    private function insert_manual_automatic_help($post, $post_id)
    {
        $html = '';

        $html .= '<div class="sssp-help-zoneid sssp-help">' . $this->help_zoneid() . '</div>';
        $html .= '<div class="sssp-help-width sssp-help">' . $this->help_width() . '</div>';
        $html .= '<div class="sssp-help-height sssp-help">' . $this->help_height() . '</div>';
        $html .= '<div class="sssp-help-automatic sssp-help">' . $this->help_automatic() . '</div>';
        $html .= '<div class="sssp-help-manual sssp-help">' . $this->help_manual($post_id) . '</div>';
        $html .= '<div class="sssp-help-class sssp-help">' . $this->help_own_class() . '</div>';
        $html .= '<div class="sssp-help-special sssp-help">' . $this->help_special_settings() . '</div>';

        return $html;
    }

    /**
     * @return array
     */
    public static function get_standard_ad_positions()
    {
        return [
            'header' => [
                'name' => esc_attr__('Header', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('The ad on this position can be inserted automatically, or manually. If you place this position automatically, be sure that your theme follows all WordPress rules for version 5.2 and newer. You can check that by yourself – simply open your theme file header.php in Editor and check if the code &#x3C;? php wp_body_open(); ?&#x3E; is inserted just below the open &#x3C;body&#x3E; tag. If not, just put the code here.', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('The ad on this position can be inserted automatically, or manually. If you place this position automatically, be sure that your theme follows all WordPress rules for version 5.2 and newer. You can check that by yourself – simply open your theme file header.php in Editor and check if the code &#x3C;? php wp_body_open(); ?&#x3E; is inserted just below the open &#x3C;body&#x3E; tag. If not, just put the code here.', SEZNAM_SSP_SLUG),
            ],
            'headerb' => [
                'name' => esc_attr__('Header or Branding', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('The same rules apply to this position as to the “Header”, however, “Branding” ad is delivered to our contractors only. If you are unsure if your contract includes this option, then it is probably unavailable to you!', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('This position cannot be visible on mobile devices.', SEZNAM_SSP_SLUG),
            ],
            'footer' => [
                'name' => esc_attr__('Footer', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('“Footer” position is placed just above the footer. It can be inserted manually or automatically.', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('“Footer” position is placed just above the footer. It can be inserted manually or automatically.', SEZNAM_SSP_SLUG),
            ],
            'inarticle' => [
                'name' => esc_attr__('In Article', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('This position can only be created automatically. It is rendered if the number of paragraphs is greater than or equal to the number of paragraphs defined in settings + 2. This means that if you choose 3 paragraphs, your article must consist of at least 5 paragraphs to make your ad appear.', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('This position can only be created automatically. It is rendered if the number of paragraphs is greater than or equal to the number of paragraphs defined in settings + 2. This means that if you choose 3 paragraphs, your article must consist of at least 5 paragraphs to make your ad appear.', SEZNAM_SSP_SLUG),
            ],
            'below_a' => [
                'name' => esc_attr__('Below the Article', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('This position can be inserted manually or automatically.', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('This position can be inserted manually or automatically.', SEZNAM_SSP_SLUG),
            ],
            'popup' => [
                'name' => esc_attr__('Pop-Up', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('This position has some limitations. It can only be inserted automatically, and on the desktop, it can only be used with the 728 × 90 px format. The position must not be shown to visitors coming from Newsfeed. If the Pop-up ad is not visible to you, your theme is apparently incompatible with it or there is an HTML error!', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('Pop-up ad can only be inserted automatically using 320 × 100 px format. If the Pop-up ad is not visible to you, your theme is apparently incompatible with it or there is an HTML error!', SEZNAM_SSP_SLUG),
            ],
            'widget' => [
                'name' => esc_attr__('Widget', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('Widget position can only be inserted to the content by our widget in the “Widgets” section, or using a special editor (if this editor has the necessary functions).', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('Widget position can only be inserted to the content by our widget in the “Widgets” section, or using a special editor (if this editor has the necessary functions).', SEZNAM_SSP_SLUG),
            ],
            'other' => [
                'name' => esc_attr__('Other', SEZNAM_SSP_SLUG),
                'description' => esc_attr__('This position can only be inserted manually by shortcode placed into any part of your theme.', SEZNAM_SSP_SLUG),
                'description-m' => esc_attr__('This position can only be inserted manually by shortcode placed into any part of your theme.', SEZNAM_SSP_SLUG),
            ],
        ];
    }

    /**
     * @return string
     */
    private function help_manual($post_id)
    {
        $html = '<h4>' . esc_attr__('Insert ad manually', SEZNAM_SSP_SLUG) . '</h4>';
        $html .= esc_attr__('If you want to insert an ad manually use one of the codes in the box "How to insert an ad into the content in manual mode?"', SEZNAM_SSP_SLUG);
        return $html;
    }

    /**
     * @return string
     */
    private function help_automatic()
    {
        $html = '<h4>' . esc_attr__('Insert ad automatically', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('There are thousands of WordPress themes – some are updated and other are not. Some themes do not support all required WordPress functions, which may disable automatic ad positions insertion. Just check if the automatic insertion was successful and if necessary, please use a manual insertion instead.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private function help_zoneid()
    {
        $html = '<h4>' . esc_attr__('Zone ID', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('Zone ID is an ID of your ad created in your Seznam Partner account.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private function help_width()
    {
        $html = '<h4>' . esc_attr__('Width', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('Width is a required parameter that specifies the maximum width of the ad element. This value must match the width of the format set in the zone detail in the Seznam Partner interface.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }


    /**
     * @return string
     */
    private function help_height()
    {
        $html = '<h4>' . esc_attr__('Height', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('Height is a required parameter that specifies the maximum height of the ad element. This value must match the height of the format set in the zone detail in the Seznam Partner interface.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private function help_own_class()
    {
        $html = '<h4>' . esc_attr__('Add your own class', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('You can add multiple classes separated by spaces. Example: my-class-1 my-class-2 my-class-3.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private function help_special_settings()
    {
        $html = '<h4>' . esc_attr__('Special settings', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('You can display ads in two different layouts. One is for standard ads displaying and the second is layout for visitors from Newsfeed only. Ad will be working only in one of those layouts.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private function help_special_settings_small()
    {
        $html = '';
        $format = html_entity_decode(esc_attr__('You can display ads in two different layouts. One is for standard ads displaying and the second is layout for visitors from Newsfeed only. Ad will be working only in one of those layouts.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * Save image change
     * @param $post_id
     *
     */
    public function item_save($post_id)
    {

        if (current_user_can(SEZNAM_SSP_CAPABILITY) && isset($_POST['sssp-zone-id'])) {
            //Sanitize Post
            $_SANITIZED_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            if (isset($_SANITIZED_POST['sssp-zone-id'])) {
                //Just fill the type if is not possible to set
                if (!isset($_SANITIZED_POST['sssp-ad-type'])) {
                    if ($_SANITIZED_POST['sssp-zone-position'] == 'headerb') {
                        $_SANITIZED_POST['sssp-ad-type'] = 'desktop';
                    }
                    if ($_SANITIZED_POST['sssp-zone-position'] == 'popup') {
                        $_SANITIZED_POST['sssp-ad-type'] = 'mobile';
                    }
                }
            }

            update_post_meta($post_id, 'sssp_ad_meta', base64_encode(serialize($_SANITIZED_POST)));
            foreach ($_SANITIZED_POST as $meta_key => $meta_value) {

                //Special rule only for arrays and mostly for checkboxes
                if (is_array($meta_value)) {
                    $meta_value = serialize($meta_value);
                }

                $find = 'sssp';
                $pos = strpos($meta_key, $find);

                if ($pos !== false) {
                    update_post_meta($post_id, $meta_key, $meta_value);
                }
            }
        }


    }

    /**
     * @param $post_id
     * @param array $standard_position
     * @return string
     */
    private function get_preview_images_desktop($post_id, $standard_position = [])
    {
        $position_status = get_post_meta($post_id, 'sssp-zone-position', true);

        $html = '<div class="sssp-image-preview-box-image">';

        if ($position_status == 'headerb') {
            $with_s = 'style="display: none"';
            $withb_s = '';
        } else {
            $with_s = '';
            $withb_s = 'style="display: none"';
        }


        $html .= '<a ' . $with_s . ' class="thickbox sssp-img-withoutbranding sssp-preview-image" href="' . SEZNAM_SSP_URL . 'assets/img/withoutbranding.png"><img alt="" src="' . SEZNAM_SSP_URL . 'assets/img/withoutbranding_300.png"></a>';

        $html .= '<a ' . $withb_s . ' class="thickbox sssp-img-withbranding sssp-preview-image" href="' . SEZNAM_SSP_URL . 'assets/img/withbranding.png"><img alt="" src="' . SEZNAM_SSP_URL . 'assets/img/withbranding_300.png"></a>';

        $html .= '</div>'; //sssp-image-preview-box-desktop
        return $html;

    }

    /**
     * @param $post_id
     * @param array $standard_position
     * @return string
     */
    private function get_preview_images_mobile()
    {

        $html = '<div class="sssp-image-preview-box-image">';

        $html .= '<a class="thickbox sssp-preview-image" href="' . SEZNAM_SSP_URL . 'assets/img/mobil-mockup.png"><img alt="" src="' . SEZNAM_SSP_URL . 'assets/img/mobil-mockup_300.png"></a>';

        $html .= '</div>'; //sssp-image-preview-box-desktop
        return $html;

    }

    /**
     * @return string
     */
    private function get_preview_text_desktop()
    {
        $standard_positions = SSSP_CustomPostEdit::get_standard_ad_positions();
        $html = '<div class="sssp-image-preview-box-text">';
        foreach ($standard_positions as $s_key => $s_value) {
            $html .= '<h3>' . $s_value['name'] . '</h3>';
            $html .= '<p>' . $s_value['description'] . '</p>';
        }
        $html .= '</div>'; //sssp-image-preview-box-text
        return $html;
    }

    /**
     * @return string
     */
    private function get_preview_text_mobile()
    {
        $standard_positions = SSSP_CustomPostEdit::get_standard_ad_positions();
        $html = '<div class="sssp-image-preview-box-text">';
        foreach ($standard_positions as $s_key => $s_value) {
            $html .= '<h3>' . $s_value['name'] . '</h3>';
            $html .= '<p>' . $s_value['description-m'] . '</p>';
        }
        $html .= '</div>'; //sssp-image-preview-box-text
        return $html;
    }

    /**
     * @return array
     */
    private function get_all_post_types()
    {

        $args = [
            'public' => true,
        ];

        $post_types_array = [];

        $output = 'objects'; // 'names' or 'objects' (default: 'names')
        $operator = 'and'; // 'and' or 'or' (default: 'and')

        $post_types = get_post_types($args, $output, $operator);

        foreach ($post_types as $post_type) {
            $post_types_array[$post_type->name] = $post_type->label;
        }

        return $post_types_array;
    }

    /**
     * @param $post
     */
    public function allowed_post_types($post)
    {

        $post_types_array = $this->get_all_post_types();
        $post_id = $post->ID;

        $html = '<div class="sssp-post-table sssp-block sssp-form">';

        $html .= '<div class="sssp-post-table-sidebar">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Allowed post types', SEZNAM_SSP_SLUG) . '*</h2></div>';

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'checkbox-array',
            'name' => 'sssp-ad-allowed-posts',
            'values' => $post_types_array,
            'value' => get_post_meta($post_id, 'sssp-ad-allowed-posts', true),
            'required' => '',
        ]);

        $html .= '<p>*' . esc_attr__('Select post types suitable for this type of ad.', SEZNAM_SSP_SLUG) . '</p>';

        $html .= '</div>'; //sssp-post-table-sidebar

        $html .= '<div class="sssp-clear"></div>';

        $html .= '</div>'; // sssp-post-table sssp-block

        echo $html;
    }

    /**
     * @param $post
     */
    public function notes_block($post)
    {
        $post_id = $post->ID;

        $html = '<div class="sssp-post-table sssp-block sssp-form">';

        $html .= '<div class="sssp-post-table-sidebar">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Notes for this ad', SEZNAM_SSP_SLUG) . '*</h2></div>';

        $notes = get_post_meta($post_id, 'sssp-notes-block', true);
        $html .= '<textarea id="sssp-notes-block" name="sssp-notes-block" style="width:100%; min-height: 150px">' . $notes . '</textarea>';

        $html .= '<p>*' . esc_attr__('A place for your personal notes on this ad or its description', SEZNAM_SSP_SLUG) . '</p>';

        $html .= '</div>'; //sssp-post-table-sidebar

        $html .= '<div class="sssp-clear"></div>';

        $html .= '</div>'; // sssp-post-table sssp-block

        echo $html;
    }

    /**
     * @param $post_id
     * @return string
     */
    private function render_html_ad($post_id)
    {

        require_once(SEZNAM_SSP_PATH . 'shortcodes/class-seznam-ads-shortcode.php'); //GP Contest Shortcode

        $ad_meta = SSSP_Main::get_ad_meta($post_id);

        $html = '';

        switch ($ad_meta['sssp-zone-position']) {
            case 'popup':
            case 'headerb':
            case 'inarticle':
                $html .= '<div class="sssp-deprecated-ads">' . esc_attr__('This type of ad cannot be displayed with the HTML code', SEZNAM_SSP_SLUG) . '</div>';
                break;
            default:
                $height = ', "height": ' . $ad_meta['sssp-zone-height'];
                $class = SSSP_Shortcode::get_class($ad_meta);
                $style = SSSP_Shortcode::get_style($ad_meta);

                $html .= '<div id="sssp_ad_' . $post_id . '_' . rand(0, 889) . '" ' . $class . $style . '>';
                $html .= '<div id="sssp_ad_' . $ad_meta['sssp-zone-id'] . '_' . rand(0, 889) . '" data-szn-ssp-ad=\'{"zoneId": ' . $ad_meta['sssp-zone-id'] . ', "width": ' . $ad_meta['sssp-zone-width'] . $height . '}\'></div>';
                $html .= '<div class="sssp-clear"></div></div>';
                $html .= '<textarea class="sssp-textarea-preview sssp-textarea-preview-html">' . htmlentities($html) . '</textarea>';
                $html .= '<div class="sssp-width-100 sssp-form">';
                $html .= '<div class="sssp-form-button-box-2" data-target="html">';
                $html .= SSSP_FormElements::get_form_field([
                    'type' => 'submit',
                    'name' => 'sssp-copy-code',
                    'label' => esc_attr__('Copy code', SEZNAM_SSP_SLUG),
                ]);
                $html .= '</div>';// sssp-form-button-box
                $html .= '</div>';
                break;
        }

        return $html;

    }

}