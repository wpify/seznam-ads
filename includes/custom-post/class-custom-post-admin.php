<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_CustomPostAdmin
{
    /**
     * Constructor
     *
     * @since     1.0.0
     */
    public function __construct()
    {


    }

    /**
     *
     */
    public static function display_admin_page()
    {
        if (isset($_POST['sssp-admin-save']) || isset($_POST['sssp-branding-save']) || isset($_POST['sssp-popup-save'])) {
            self::save_admin_page();
        }

        add_thickbox();

        $html = '<div class="wrap sssp-post-table sssp-block" style="max-width: 1600px">';
        $html .= '<h1>' . esc_attr__('Seznam Reklama WP plugin – General settings', SEZNAM_SSP_SLUG) . '</h1>';
        $html .= '<div class="sssp-post-table">';


        $html .= '<div class="sssp-post-table-left">';

        $html .= self::block_settings();
        $html .= self::branding_settings();
        $html .= self::popup_settings();
        $html .= self::block_system_checker();
        $html .= self::block_compatibility_checker();
        $html .= self::block_cache_plugin_checker();

        $html .= '</div>';// sssp-post-table-left


        $html .= '<div class="sssp-post-table-right">';

        $html .= self::block_help();

        $html .= '</div>';// sssp-post-table-right


        $html .= '</div>';// sssp-post-table
        $html .= '</div>';// wrap

        echo $html;
    }

    /**
     * @return string
     */
    private static function block_settings()
    {

        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('General settings', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= '<form method="post" action="" class="sssp-form">';

        $html .= '<div class="sssp-width-50">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-allowed-capability',
            'label' => esc_attr__('Restrict access to the plugin to', SEZNAM_SSP_SLUG) . ':',
            'options' => [
                'manage_options' => esc_attr__('Administrators (capability: manage_options)', SEZNAM_SSP_SLUG),
                'edit_pages' => esc_attr__('Administrators, Editors (capability: edit_pages)', SEZNAM_SSP_SLUG),
                'edit_posts' => esc_attr__('Administrators, Editors, Authors (capability: edit_posts)', SEZNAM_SSP_SLUG),
                get_option('sssp-allowed-capability-special', 'publish_posts') => esc_attr__('Specific user capability', SEZNAM_SSP_SLUG),
            ],
            'value' => get_option('sssp-allowed-capability', 'manage_options'),
            'required' => '',
            'status' => '',
            'default' => 'center',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-50">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'select',
            'name' => 'sssp-allowed-capability-special',
            'label' => esc_attr__('Select special capability', SEZNAM_SSP_SLUG),
            'options' => self::create_capabilities_array(),
            'value' => get_option('sssp-allowed-capability-special', 'publish_posts'),
            'required' => '',
            'status' => 'disabled',
            'default' => 'center',
        ]);
        $html .= '</div>';

        $html .= '<div class="sssp-width-50">';

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'number',
            'name' => 'sssp-mobile-breakpoint',
            'label' => esc_attr__('Mobile breakpoint (value in pixels)', SEZNAM_SSP_SLUG),
            'value' => get_option('sssp-mobile-breakpoint', '700'),
            'min' => '600',
            'max' => '970',
            'step' => '1',
            'required' => 'required',
            'status' => '',
            'placeholder' => '',
            'default' => '700',
        ]);

        $html .= '</div>';

        $html .= '<div class="sssp-width-50 sssp-mobile">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Special settings', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '</div>';

        $html .= '<div class="sssp-width-50" style="vertical-align: bottom">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'checkbox',
            'name' => 'sssp-media-contract',
            'label' => esc_attr__('We have an agreement on media representation by Seznam.cz, a. s.', SEZNAM_SSP_SLUG),
            'value' => get_option('sssp-media-contract', '0'),
            'required' => '',
        ]);
        $html .= '</div>';


        $html .= '<div class="sssp-clear"></div>';

        $html .= '<div class="sssp-width-100">';
        $html .= '<div class="sssp-form-button-box">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'submit',
            'name' => 'sssp-admin-save',
            'label' => esc_attr__('Save settings', SEZNAM_SSP_SLUG),
        ]);
        $html .= '</div>';// sssp-form-button-box
        $html .= '</div>';

        $html .= '</form>';

        $html .= '</div>';// sssp-settings-box

        return $html;

    }

    /**
     * @return string
     */
    private static function branding_settings()
    {
        //sssp-branding-css
        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Branding settings', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '<p>' . esc_attr__('Branding cannot be perfectly placed automatically to every single theme – you must set its position manually using CSS. If automatic Branding insertion is quite impossible you must do it manually by yourself.', SEZNAM_SSP_SLUG) . '</p>';

        $html .= '<form id="sssp-branding-css-form" method="post" class="sssp-form">';

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'textarea',
            'name' => 'sssp-branding-css',
            'label' => esc_attr__('CSS for Branding', SEZNAM_SSP_SLUG),
            'value' => get_option('sssp-branding-css', self::custom_css_branding()),
        ]);

        $html .= '<div class="sssp-width-100">';
        $html .= '<div class="sssp-form-button-box">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'submit',
            'name' => 'sssp-branding-save',
            'label' => esc_attr__('Save Branding settings', SEZNAM_SSP_SLUG),
        ]);
        $html .= '</div>';// sssp-form-button-box
        $html .= '</div>';

        $html .= '</form>';

        $html .= '</div>';// sssp-settings-box

        return $html;
    }

    /**
     * @return string
     */
    private static function popup_settings()
    {
        //sssp-branding-css
        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Pop-up settings', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= '<p>' . esc_attr__('This setting should be managed by advanced users only!', SEZNAM_SSP_SLUG) . '</p>';

        $html .= '<form id="sssp-popup-css-form" method="post" class="sssp-form">';

        $html .= SSSP_FormElements::get_form_field([
            'type' => 'textarea',
            'name' => 'sssp-popup-css',
            'label' => esc_attr__('CSS for Pop-up', SEZNAM_SSP_SLUG),
            'value' => get_option('sssp-popup-css', self::custom_css_popup()),
        ]);

        $html .= '<div class="sssp-width-100">';
        $html .= '<div class="sssp-form-button-box">';
        $html .= SSSP_FormElements::get_form_field([
            'type' => 'submit',
            'name' => 'sssp-popup-save',
            'label' => esc_attr__('Save pop-up settings', SEZNAM_SSP_SLUG),
        ]);
        $html .= '</div>';// sssp-form-button-box
        $html .= '</div>';

        $html .= '</form>';

        $html .= '</div>';// sssp-settings-box

        return $html;
    }

    /**
     * @return string
     */
    private static function block_help()
    {
        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Help', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= self::capability_filter_help();
        $html .= self::mobile_breakpoint_help();
        $html .= self::media_contract_help();


        $html .= '</div>';// sssp-settings-box

        $html .= '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Fix compatibility', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= self::mobile_header_compatibility_help();

        $html .= '</div>';// sssp-settings-box

        $html .= '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('System requirements', SEZNAM_SSP_SLUG) . '</h2></div>';
        $html .= self::block_requirements();

        $html .= '</div>';// sssp-settings-box

        return $html;

    }

    /**
     * @return string
     */
    private static function block_system_checker()
    {
        global $wp_version;
        $theme = wp_get_theme();

        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('System check', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('WordPress version', SEZNAM_SSP_SLUG) . ':</span> ' . $wp_version . '</div>';
        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('Current PHP version', SEZNAM_SSP_SLUG) . ':</span> ' . phpversion() . '</div>';
        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('Seznam SSP plugin version', SEZNAM_SSP_SLUG) . ':</span> ' . SEZNAM_SSP_VERSION . '</div>';
        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('Theme name (version)', SEZNAM_SSP_SLUG) . ':</span> ' . $theme->get('Name') . ' (' . $theme->get('Version') . ')</div>';

        $html .= '</div>';// sssp-settings-box

        return $html;

    }

    /**
     * @return string
     */
    private static function block_compatibility_checker()
    {
        $theme = wp_get_theme();

        $header_file_link = get_template_directory() . '/header.php';
        $header_file_exists = file_exists($header_file_link) ? ('<span class="sssp-admin-green">' . esc_attr__('Yes', SEZNAM_SSP_SLUG) . '</span>') : ('<span class="sssp-admin-red">' . esc_attr__('No', SEZNAM_SSP_SLUG) . '</span>');
        $edit_file_link = SEZNAM_SSP_ADMIN_URL . 'theme-editor.php?file=header.php&theme=' . $theme->get('TextDomain');
        $get_header_file = file_get_contents($header_file_link);

        $find = 'wp_body_open(';
        $check = strpos($get_header_file, $find);
        if ($check !== false) {
            $check_message = '<span class="sssp-admin-green">' . esc_attr__('Compatible', SEZNAM_SSP_SLUG) . '</span>';
        } else {
            $check_message = '<span class="sssp-admin-red">' . esc_attr__('Not compatible!', SEZNAM_SSP_SLUG) . '</span>';
        }

        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Compatibility check', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('Standard header file exists', SEZNAM_SSP_SLUG) . ':</span> ' . $header_file_exists . '</div>';
        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('Header compatibility with WP 5.2+', SEZNAM_SSP_SLUG) . ':</span> ' . $check_message . '</div>';
        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('Header edit link', SEZNAM_SSP_SLUG) . ':</span> <a href="' . $edit_file_link . '">' . esc_attr__('Link to editor', SEZNAM_SSP_SLUG) . '</a></div>';

        $html .= '</div>';// sssp-settings-box

        return $html;
    }

    /**
     * @return string
     */
    private static function block_cache_plugin_checker()
    {
        $html = '<div class="sssp-settings-box">';
        $html .= '<div class="sssp-section-title"><h2>' . esc_attr__('Cache plugin checker', SEZNAM_SSP_SLUG) . '</h2></div>';

        $html_plugins = '';

        if (defined('W3TC')) {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-green sssp-admin-bold">W3 Total Cache ' . esc_attr__('plugin detected', SEZNAM_SSP_SLUG) . '</span></div>';
        } else {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-black">W3 Total Cache ' . esc_attr__('plugin not detected', SEZNAM_SSP_SLUG) . '</span></div>';
        }

        if (defined('WPFC_WP_PLUGIN_DIR')) {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-green sssp-admin-bold">WP Fastest Cache ' . esc_attr__('plugin detected', SEZNAM_SSP_SLUG) . '</span></div>';
        } else {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-black">WP Fastest Cache ' . esc_attr__('plugin not detected', SEZNAM_SSP_SLUG) . '</span></div>';
        }

        if (defined('WPCACHEHOME')) {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-green sssp-admin-bold">WP Super Cache ' . esc_attr__('plugin detected', SEZNAM_SSP_SLUG) . '</span></div>';
        } else {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-black">WP Super Cache ' . esc_attr__('plugin not detected', SEZNAM_SSP_SLUG) . '</span></div>';
        }

        if (defined('WPO_VERSION')) {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-green sssp-admin-bold">WP Optimize ' . esc_attr__('plugin detected', SEZNAM_SSP_SLUG) . '</span></div>';
        } else {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-black">WP Optimize ' . esc_attr__('plugin not detected', SEZNAM_SSP_SLUG) . '</span></div>';
        }

        if (defined('LSCWP_V')) {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-green sssp-admin-bold">LiteSpeed Cache ' . esc_attr__('plugin detected', SEZNAM_SSP_SLUG) . '</span></div>';
        } else {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-black">LiteSpeed Cache ' . esc_attr__('plugin not detected', SEZNAM_SSP_SLUG) . '</span></div>';
        }

        if (defined('WCL_PLUGIN_ACTIVE')) {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-green sssp-admin-bold">Clearfy ' . esc_attr__('plugin detected', SEZNAM_SSP_SLUG) . '</span></div>';
        } else {
            $html_plugins .= '<div class="sssp-system-check-row"><span class="sssp-admin-black">Clearfy ' . esc_attr__('plugin not detected', SEZNAM_SSP_SLUG) . '</span></div>';
        }

        $html .= $html_plugins . '</div>';// sssp-settings-box

        return $html;
    }

    /**
     * @return string
     */
    private static function mobile_breakpoint_help()
    {
        $html = '<h4>' . esc_attr__('Mobile breakpoint', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('This value represents the maximum width breakpoint between desktop and mobile ad types. Ads for mobile devices will only be visible if the page width is less than or equal to this breakpoint. Its value should be in the range of 600 to 970 pixels. The recommended value is 700 pixels.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private static function media_contract_help()
    {
        $html = '<h4>' . esc_attr__('Media representation by Seznam.cz, a. s.', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('Some companies choose to by represented by Seznam, a. s. on media market. If you have such an agreement with Seznam.cz, a. s. please do check the checkbox “Media representation by Seznam.cz, a. s.”.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private static function capability_filter_help()
    {
        $html = '<h4>' . esc_attr__('Restrict access to the plugin to', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('You have an option to restrict access to the plugin interface. It is quite important to be careful if you select the “Specific user capability” option. This option gives you more flexibility in choosing who gets access to the plugin, but can also make it available to inappropriate people. Therefore, only a very small group of very experienced users should manage this setting.', SEZNAM_SSP_SLUG));
        $html .= $format;
        return $html;
    }

    /**
     * @return string
     */
    private static function mobile_header_compatibility_help()
    {
        $html = '<h4>' . esc_attr__('Header compatibility with WP 5.2', SEZNAM_SSP_SLUG) . '</h4>';
        $format = html_entity_decode(esc_attr__('Header compatibility with WP is crucial for the automatic insertion of "Header" and "Header or Branding" ad positions. This compatibility is very easy to achieve with the following procedure:', SEZNAM_SSP_SLUG));
        $format .= ' <br>' . sprintf(esc_html__('Please add this piece of code “%s” to the header.php file after the “%s” tag.', SEZNAM_SSP_SLUG), '&#x3C;?php wp_body_open(); ?&#x3E;', '&#x3C;body &#x3C;?php body_class(); ?&#x3E;&#x3E;') . '';
        $html .= $format;
        $html .= '<a class="thickbox sssp-img-withbranding sssp-preview-image" href="' . SEZNAM_SSP_URL . 'assets/img/wp-body-open.png"><img alt="" class="sssp-wp-body-example" src="' . SEZNAM_SSP_URL . 'assets/img/wp-body-open.png" title="' . esc_attr__('Example', SEZNAM_SSP_SLUG) . '"></a>';
        return $html;
    }

    /**
     * @return string
     */
    private static function block_requirements()
    {

        $html = '<div class="sssp-system-check-row"><span>' . esc_attr__('WordPress version', SEZNAM_SSP_SLUG) . ':</span> 5.2 +</div>';
        $html .= '<div class="sssp-system-check-row"><span>' . esc_attr__('PHP version', SEZNAM_SSP_SLUG) . ':</span> 5.6 +</div>';
        return $html;

    }

    /**
     * @return bool[]
     */
    private static function create_capabilities_array()
    {
        $capabilities = get_role('administrator')->capabilities;

        unset($capabilities['manage_options']);
        unset($capabilities['edit_pages']);
        unset($capabilities['edit_posts']);
        foreach ($capabilities as $key => $value) {
            $capabilities[$key] = $key;
        }

        return $capabilities;
    }

    /**
     * @return string
     */
    public static function custom_css_branding()
    {
        return '/* styl definující umístění elementu určeného pro vykreslení brandingu k hornímu okraji okna prohlížeče a vodorovně uprostřed okna prohlížeče */
#ssp-zone-header-branding {
	position: fixed;
	left: 50%;
	transform: translateX(-50%);
	margin: 0 auto;
}
/* definice vlastností třídy adFull, která se elementu s brandingem přiřadí v okamžiku, kdy je do něj úspěšně vykreslena reklama; určuje šířku (2000 px) a výšku (1400 px) elementu s brandingem */
#ssp-zone-header-branding.adFull {
	width: 2000px;
	height: 1400px;
}
/* příklad definice odsazení těla stránky (předpokládáme umístění v elementu div#page) o 226 px od horního okraje po vykreslení brandingu */
#ssp-zone-header-branding.adFull ~ #page {
	position: relative;
	top: 226px;
}';
    }

    /**
     * @return string
     */
    public static function custom_css_popup()
    {
        return '/* Styl pro plochu určenou k zobrazování Pop-Upu */
#sssp-pop-up-ad {
    position: fixed;
    height: auto;
    width: 100%;
    background: rgba(0, 0, 0, .7);
    bottom: 0;
    left: 0;
    right: 0;
    text-align: center;
    padding: 2px 0;
    z-index: 1001;
    transition: height 0.5s;
	margin:0;
}
/* Tlačítko pro zavření reklamy */
#sssp-pop-up-ad-close {
    background: rgba(0, 0, 0, 1);
    margin-top: -22px;
    position: absolute;
    top: 0;
    right: 0;
    color: #CCC;
    cursor: pointer;
	font-size:14px;
    text-align: center;
    padding: 4px 8px;
    height: 22px;
	line-height:16px;
	font-family: Arial, Helvetica, sans-serif;
}
.sssp-pop-up-ad-hide {
    height: 0 !important;
    padding: 0 !important;
    margin: 0 !important;
}';
    }

    /**
     *
     */
    public static function save_admin_page()
    {
        if (current_user_can(SEZNAM_SSP_CAPABILITY)) {
            $_SANITIZED_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            //Media representation contract checker
            if(isset($_POST['sssp-admin-save'])){
                unset($_SANITIZED_POST['sssp-admin-save']);
                if(isset($_SANITIZED_POST['sssp-media-contract'])){
                    $_SANITIZED_POST['sssp-media-contract'] = '1';
                }else{
                    $_SANITIZED_POST['sssp-media-contract'] = '0';
                }
            }

            foreach ($_SANITIZED_POST as $post_key => $post_value) {
                update_option($post_key, $post_value, true);
            }
        }

    }

}