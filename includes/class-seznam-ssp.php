<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_Main
{

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Constructor
     *
     * @since     1.0.0
     */

    public function __construct()
    {
        // Load plugin text domain
        add_action('init', [$this, 'load_plugin_text_domain']);
        // Activate plugin when new blog is added
        add_action('wpmu_new_blog', [$this, 'activate_new_site']);
        // Add styles
        add_action('wp_enqueue_scripts', [$this, 'enqueue_styles']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);

        add_action('widgets_init', [$this, 'sssp_load_widget']);

        //Media representation header code
        if (get_option('sssp-media-contract', '0') == 1) {
            add_action('wp_head', [$this, 'media_representation_header_code']);
        }


        //Set development cookie
        $this->set_dev_cookie();

    }

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Load the plugin text domain for translation.
     */
    public function load_plugin_text_domain()
    {
        $domain = SEZNAM_SSP_SLUG;
        $locale = apply_filters('plugin_locale', get_locale(), $domain);

        load_textdomain($domain, trailingslashit(WP_LANG_DIR) . $domain . '/' . $domain . '-' . $locale . '.mo');
        load_plugin_textdomain($domain, FALSE, basename(dirname(__DIR__)) . '/languages/');

    }

    /**
     * Fired when a new site is activated with a WPMU environment.
     *
     * @param int $blog_id ID of the new blog.
     * @since    1.0.0
     *
     */
    public function activate_new_site($blog_id)
    {
        if (1 !== did_action('wpmu_new_blog')) {
            return;
        }

        switch_to_blog($blog_id);
        self::single_activate();
        restore_current_blog();
    }

    /**
     * Fired when the plugin is activated.
     *
     * @param boolean $network_wide True if WPMU superadmin uses
     * "Network Activate" action, false if
     *  WPMU is disabled or plugin is
     *  activated on an individual blog.
     * @since    1.0.0
     *
     */
    public static function activate($network_wide)
    {

        if (function_exists('is_multisite') && is_multisite()) {

            global $wpdb;

            if ($network_wide) {

                // Get all blog ids
                $blog_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");

                foreach ($blog_ids as $blog_id) {

                    switch_to_blog($blog_id);
                    self::single_activate();
                }

                restore_current_blog();

            } else {
                self::single_activate();
            }

        } else {
            self::single_activate();
        }

    }

    /**
     * Fired when the plugin is deactivated.
     *
     * @param boolean $network_wide True if WPMU superadmin uses
     * "Network Deactivate" action, false if
     *  WPMU is disabled or plugin is
     *  deactivated on an individual blog.
     * @since    1.0.0
     *
     */
    public static function deactivate($network_wide)
    {
        if (function_exists('is_multisite') && is_multisite()) {

            global $wpdb;

            if ($network_wide) {

                // Get all blog ids
                $blog_ids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");

                foreach ($blog_ids as $blog_id) {

                    switch_to_blog($blog_id);
                    self::single_deactivate();

                }

                restore_current_blog();

            } else {
                self::single_deactivate();
            }

        } else {
            self::single_deactivate();
        }
    }

    /**
     * Fired for each blog when the plugin is activated.
     *
     * @since    1.0.0
     */
    private static function single_activate()
    {

        $args = [
            'post_type' => 'seznam-ssp', //Do not change to seznam-ads
            'posts_per_page' => -1,
        ];
        $posts = get_posts($args);

        if ($posts) {
            foreach ($posts as $single_p) {
                $my_post = [
                    'ID' => $single_p->ID,
                    'post_type' => 'seznam-ads',
                ];

                wp_update_post($my_post);

            }
        }
    }

    /**
     * Fired for each blog when the plugin is deactivated.
     *
     * @since    1.0.0
     */
    private static function single_deactivate()
    {

    }

    /**
     * Just add styles to the Wordpress
     */
    public function enqueue_styles()
    {
        wp_enqueue_style(SEZNAM_SSP_SLUG . '-style', SEZNAM_SSP_URL . 'assets/css/public.min.css', [], SEZNAM_SSP_VERSION);

    }

    /**
     *
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script(SEZNAM_SSP_SLUG . '-public', SEZNAM_SSP_URL . 'assets/js/seznam-ssp.min.js', ['jquery'], SEZNAM_SSP_VERSION, false);
        wp_enqueue_script(SEZNAM_SSP_SLUG . '-sssp', 'https://ssp.imedia.cz/static/js/ssp.js', [], SEZNAM_SSP_VERSION, false);

    }

    /**
     *
     */
    private function set_dev_cookie()
    {
        if (!is_admin()) {
            if (isset($_GET['sssp-dev']) && $_GET['sssp-dev'] == 1) {
                if (isset($_GET['sssp-dev-hash']) && md5('sssp-dev-hash') == $_GET['sssp-dev-hash']) {
                    setcookie('sssp-dev-mode', $_GET['sssp-dev-hash'], time() + 3600 * 360 * 24 * 2, COOKIEPATH, COOKIE_DOMAIN);
                } else {
                    setcookie('sssp-dev-mode', '1', time() + 3600 * 360 * 24 * 2, COOKIEPATH, COOKIE_DOMAIN);
                }
            } elseif (isset($_GET['sssp-dev']) && empty($_GET['sssp-dev'])) {
                setcookie('sssp-dev-mode', '0', time() - 3600, COOKIEPATH, COOKIE_DOMAIN);
            }
        }
    }

    /**
     *
     */
    public function sssp_load_widget()
    {
        register_widget('SSSP_RegisterWidget');
    }

    /**
     * @return string
     */
    public static function check_ssp_newsfeed()
    {

        if (isset($_GET['utm_source']) && isset($_GET['utm_medium']) && ($_GET['utm_source'] == 'www.seznam.cz' && $_GET['utm_medium'] == 'sekce-z-internetu')) {
            return '1';
        }

        if (isset($_COOKIE[SEZNAM_SSP_NWSFD_COOKIE])) {
            return '1';
        }

        return '0';
    }

    /**
     *
     */
    public function media_representation_header_code()
    {
        $html = '<script type="text/javascript">';
        $html .= 'sssp.config({
        source: "media"
        });';
        $html .= '</script>';

        echo $html;

    }

    /**
     * @param $post_id
     * @return mixed|void
     */
    public static function get_ad_meta($post_id)
    {

        $ad_meta_encoded = get_post_meta($post_id, 'sssp_ad_meta', true);
        if (base64_encode(base64_decode($ad_meta_encoded, true)) === $ad_meta_encoded) {
            return unserialize(base64_decode($ad_meta_encoded));
        } else {
            return unserialize($ad_meta_encoded);
        }
    }


}