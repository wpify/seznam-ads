jQuery(function ($) {

    /**RUN ADS**/
    jQuery(document).ready(function () { //Verify that document is complete
        if (typeof sssp.getAdsByTags === "function" && typeof sssp_get_breakpoint === "function") {
            if (window.innerWidth <= sssp_get_breakpoint()) { //Check if the windows is lower or equal to breakpoint
                jQuery.when(jQuery('.sssp-seznam-ad-desktop').remove()).then(sssp.getAdsByTags()); //Remove desktop ADS
            } else {
                jQuery.when(jQuery('.sssp-seznam-ad-mobile').remove()).then(sssp.getAdsByTags()); //Remove mobile ADS
            }
        } else {
            //Just let now to the console that something is wrong (for debug purposes)
            console.warn('sssp.getAdsByTags() or sssp_get_breakpoint() is not a function!');
        }
    });

    /**ReloadADS Debug Tool**/
    jQuery(document).on("click", '#sssp-reload-ads', function () {
        jQuery('.sssp-posCont').remove();
        sssp.getAdsByTags();
    });

    /**POPUP**/
    jQuery(document).ready(function () {
        var popup_div = jQuery('#ssspShowPopUp');
        var popup_zone_id_m = popup_div.data('zoneidm');
        var popup_zone_id_d = popup_div.data('zoneidd');
        var breakpoint = popup_div.data('width');
        var mz_status = popup_div.data('mz');

        if (popup_zone_id_m !== undefined) {
            sssp_getPopUpM(541, "ssspShowPopUp", popup_zone_id_m);
        }
        if (popup_zone_id_d !== undefined && mz_status === 0) {
            sssp_getPopUpD(breakpoint, "ssspShowPopUp", popup_zone_id_d);
        }
        if (popup_zone_id_d !== undefined && popup_zone_id_m !== undefined && mz_status === 1 && sssp.displaySeznamAds() === false) {
            sssp_getMzPopUp('ssspShowPopUp', popup_zone_id_m, popup_zone_id_d);
        }
    })

    /**
     *
     * @param cname
     * @returns {string}
     */
    function sssp_getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /**
     *
     * @param value
     */
    //Set cookie with 30 min expiration
    function sssp_setCookie(value) {
        var d = new Date();
        d.setTime(d.getTime() + (30 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "sssp-pop-up-ad-first-look=" + value + ";" + expires + ";path=/";
    }

    /**
     *
     * @param zoneId
     */
    function sssp_createAdDivs(zoneId) {
        var div = document.createElement('div');
        div.setAttribute('id', 'sssp-pop-up-ad');
        div.innerHTML = ('<div id="sssp-pop-up-ad-close">Zavřít reklamu</div>' + '<div id="ssp-zone-' + zoneId + '" style="margin: 0 auto;"></div>');
        document.body.appendChild(div);
    }

    /**
     * Hide Ad
     */
    function sssp_hideAd() {
        document.getElementById("sssp-pop-up-ad").classList.add("sssp-pop-up-ad-hide");
        document.getElementById("sssp-pop-up-ad-close").style.display = "none";
    }

    /**
     *
     * @param breakPoint
     * @param onScrollElementId
     * @param zoneId
     */
    function sssp_getPopUpM(breakPoint, onScrollElementId, zoneId) {

        if (window.innerWidth <= breakPoint && document.getElementById(onScrollElementId)) {
            if (!sssp_getCookie("sssp-pop-up-ad-first-look")) {
                sssp_setCookie(false);
            }
            window.addEventListener("scroll", function () {
                var createdAd = document.getElementById("sssp-pop-up-ad");
                var elementTarget = document.getElementById(onScrollElementId);
                if (window.scrollY > (elementTarget.offsetTop - window.innerHeight)) {
                    if (!createdAd && sssp_getCookie("sssp-pop-up-ad-first-look") === "false") {
                        sssp_setCookie(true);
                        sssp_createAdDivs(zoneId);
                        sssp.getAds([
                            {
                                "zoneId": zoneId,
                                "id": "ssp-zone-" + zoneId,
                                "width": 320,
                                "height": 100,
                                options: {
                                    infoCallback: (ad) => {
                                        if (ad.type === 'empty' || ad.type === 'error') {
                                            document.getElementById("sssp-pop-up-ad").style.display = 'none';
                                        }
                                    }
                                }
                            }
                        ]);
                    }
                    if (document.getElementById("sssp-pop-up-ad-close")) {

                        document.getElementById("sssp-pop-up-ad-close").addEventListener("click", function () {
                            sssp_hideAd();
                        });
                    }
                }
            });
        }
    }

    /**
     *
     * @param breakPoint
     * @param onScrollElementId
     * @param zoneId
     */
    function sssp_getPopUpD(breakPoint, onScrollElementId, zoneId) {
        //Check if exist cookie with name "cname"

        if (window.innerWidth > breakPoint && document.getElementById(onScrollElementId)) {
            if (!sssp_getCookie("sssp-pop-up-ad-first-look")) {
                sssp_setCookie(false);
            }

            window.addEventListener("scroll", function () {
                var createdAd = document.getElementById("sssp-pop-up-ad");
                var elementTarget = document.getElementById(onScrollElementId);
                if (window.scrollY > (elementTarget.offsetTop - window.innerHeight)) {
                    if (!createdAd && sssp_getCookie("sssp-pop-up-ad-first-look") === "false") {
                        sssp_setCookie(true);
                        sssp_createAdDivs(zoneId);
                        sssp.getAds([
                            {
                                "zoneId": zoneId,
                                "id": "ssp-zone-" + zoneId,
                                "width": 728,
                                "height": 90,
                                options: {
                                    infoCallback: (ad) => {
                                        if (ad.type === 'empty' || ad.type === 'error') {
                                            document.getElementById("sssp-pop-up-ad").style.display = 'none';
                                        }
                                    }
                                }
                            }
                        ]);
                    }
                    if (document.getElementById("sssp-pop-up-ad-close")) {

                        document.getElementById("sssp-pop-up-ad-close").addEventListener("click", function () {
                            sssp_hideAd();
                        });
                    }
                }
            });
        }
    }

    /**
     *
     * @param onScrollElementId
     * @param mobileZoneID
     * @param desktopZoneId
     * @returns {string}
     */
    function sssp_getMzPopUp(onScrollElementId, mobileZoneID, desktopZoneId) {
        var onScrollElement = document.getElementById(onScrollElementId);
        var impressionsCounter = 0;
        var visible = 0;

        //Kontola předaných dat
        if (!onScrollElementId || !mobileZoneID || !desktopZoneId) {
            return 'Vyplńte všechna potřebná data (onScrollElementId, mobileZoneID, desktopZoneId)!'
        }

        if (!onScrollElement) {
            return 'Element s id = ' + onScrollElementId + 'nebyl nalezen!'
        }

        /**
         * Check if exist cookie with name "cname"
         * @param cname
         * @returns {string}
         */
        function sssp_mz_getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
        }

        /**
         * Set cookie with 30 min expiration
         * @param value
         */
        function sssp_mz_setCookie(value) {
            var d = new Date();
            d.setTime(d.getTime() + (30 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = "seznam--mz-pop-up-ad=" + value + ";" + expires + ";path=/";
        }

        // Cookie exist control
        if (!sssp_mz_getCookie("seznam--mz-pop-up-ad")) {
            sssp_mz_setCookie(JSON.stringify({type: 'MZ-pop-up', impressions: impressionsCounter, visible: 0}));
        } else {
            impressionsCounter = JSON.parse(sssp_mz_getCookie('seznam--mz-pop-up-ad')).impressions + 1;
        }
        // Window width control
        if (window.innerWidth < 540 || window.innerWidth > 970) {
            sssp_createAdDivs(((window.innerWidth < 540) ? mobileZoneID : desktopZoneId));
            document.getElementById("sssp-pop-up-ad").style.display = 'none';
            window.addEventListener("scroll", function () {
                //End of the article control
                if (window.scrollY > (onScrollElement.offsetTop - window.innerHeight) && visible === 0) {
                    visible = 1;
                    //Kontrola delitelnosti
                    if (impressionsCounter % 3 === 0) {
                        sssp.getAds([
                            {
                                "zoneId": ((window.innerWidth < 540) ? mobileZoneID : desktopZoneId),
                                "id": "ssp-zone-" + ((window.innerWidth < 540) ? mobileZoneID : desktopZoneId),
                                "width": ((window.innerWidth < 540) ? '320' : '728'),
                                "height": ((window.innerWidth < 540) ? '100' : '90'),
                                options: {
                                    infoCallback: (ad) => {
                                        if (ad.type === 'empty' || ad.type === 'error') {
                                            sssp_mz_setCookie(JSON.stringify({
                                                type: 'MZ-pop-up',
                                                impressions: impressionsCounter,
                                                visible: 0
                                            }));
                                        } else {
                                            document.getElementById("sssp-pop-up-ad").style.display = 'block';
                                        }
                                        sssp_mz_setCookie(JSON.stringify({
                                            type: 'MZ-pop-up',
                                            impressions: impressionsCounter,
                                            visible: 1
                                        }));
                                    }
                                }
                            }
                        ]);
                    } else {
                        sssp_mz_setCookie(JSON.stringify({
                            type: 'MZ-pop-up',
                            impressions: impressionsCounter,
                            visible: 0
                        }));
                    }
                }
            });
        }
        //Event for closing ad
        if (document.getElementById("sssp-pop-up-ad-close")) {
            document.getElementById("sssp-pop-up-ad-close").addEventListener("click", function () {
                sssp_hideAd();
            });
        }
    }

    /**
     * Sticky Widget
     */
    jQuery(document).ready(function () {

        jQuery('.sssp-sticky-box').each(function() {
            if (jQuery(this).is(":last-child")) {

            }else{
                jQuery(this).removeClass('sssp-sticky-box');
                jQuery('[id="sssp-breakpoint"]:not(#sssp-breakpoint:last)').remove();
            }
        }).promise().done( function(){
            var sticky_box = jQuery('.sssp-sticky-box');
            var sticky_box_id = jQuery('.sssp-sticky-box').attr('id');
            if (sticky_box.length) {
                sssp_sticky('sssp-breakpoint', sticky_box_id);
            }

        } );

    });

    /**
     * @param breakPoint
     * @param onScrollElementId
     */
    function sssp_sticky(breakPoint, onScrollElementId) {
        if (window.innerWidth >= 1365) {
            var sticky_start_time = jQuery.now();
            var e_element = jQuery('#' + onScrollElementId + ':last-child'); //On Scroll Widget
            var desktop_class = e_element.hasClass('sssp-seznam-ad-desktop'); //Verify that ad is for a desktop
            if (e_element && desktop_class) {

                //Set width of the element.
                var a_width = e_element.outerWidth();
                e_element.css({'width': a_width, 'box-sizing': 'border-box', 'position': 'static'});

                //Set basic offset before sticky
                var y_end_offset = jQuery('#sssp-content-end').offset(); //Offset of the div on the end of content


                var y_e_offset = e_element.offset(); //Scrolling element offset
                var c_height = jQuery('#comments').outerHeight(); //comments section height
                var d_height = jQuery('#sdop-below-article-box').outerHeight(); //Recommendation box height


                if (c_height === undefined) {
                    c_height = 0; //set comments box height to zero;
                    if (d_height !== undefined) {
                        y_end_offset = jQuery('#sdop-below-article-box').offset();//Offset of the recommendation box
                    }
                } else {
                    y_end_offset = jQuery('#comments').offset(); //Offset of the div on the end of content
                }

                if (y_end_offset.top + c_height > y_e_offset.top + e_element.outerHeight()) { //Verify that scrolling is even possible before make you element sticky

                    var widget_object = e_element.find('.sssp-seznam-ad-desktop').data('ssp-ad-object');
                    var widget_id = e_element.find('.sssp-seznam-ad-desktop').data('ssp-ad-id');
                    var adOffset = jQuery('#' + widget_id).outerHeight();
                    var sssp_flag_one = true;
                    var sssp_flag_two = false;
                    var sssp_flag_three = false;

                    jQuery(window).scroll(function () {
                        var e_offset = e_element.offset(); //Scrolling element offset
                        var b_offset = jQuery('#' + breakPoint).offset(); //Breakpoint offset
                        var top = jQuery(window).scrollTop();
                        var end_offset = jQuery('#sssp-content-end').offset(); //Offset of the div on the end of content
                        var a_height = e_element.outerHeight(); //Scrolling element height
                        var sticky_current_time = jQuery.now();
                        var c_height = jQuery('#comments').outerHeight(); //comments section height

                        if (c_height === undefined) {
                            c_height = 0;
                        } else {
                            end_offset = jQuery('#comments').offset(); //Offset of the div on the end of content
                        }

                        //Set the end of the element for sticky position
                        var end_offset_status = e_offset.top > end_offset.top - a_height + c_height;


                        //Just set basic non-sticky statement
                        var ssspsticky = false;

                        //Set basic left offset. It is important for the right position sticky element
                        e_element.css({'left': e_offset.left, 'position': 'fixed'});


                        if (e_offset && end_offset) { //Verify that both parameters are on the page before you start make sticky
                            if (b_offset.top < top && end_offset.top > e_offset.top) {
                                e_element.addClass('sssp-sticky');
                                if (sssp_isInViewport('#sssp-content-end')) { //verify that element #sssp-content-end is in viewport
                                    if (end_offset_status) { //IF Element reach the end
                                        e_element.css({'visibility': 'hidden'});
                                        ssspsticky = false;
                                    } else {
                                        e_element.css({'visibility': 'visible'});
                                        ssspsticky = true;
                                    }

                                } else {
                                    e_element.css({'visibility': 'visible'});
                                    ssspsticky = true;
                                }
                            } else if (b_offset.top < top && end_offset.top < e_offset.top) { //If is the Widget bellow the content but is there comments and space for scrolling
                                if (end_offset_status) { //IF Element reach the end
                                    e_element.css({'visibility': 'hidden'});
                                    ssspsticky = false;
                                } else {
                                    e_element.css({'visibility': 'visible'});
                                    ssspsticky = true;
                                }
                            } else {
                                e_element.removeClass('sssp-sticky').css({'position': 'static'});
                                ssspsticky = false;
                            }
                        }

                        //Remove sticky if is resized window
                        jQuery(window).resize(function () {
                            if (ssspsticky === true) {
                                e_element.removeClass('sssp-sticky').css({'position': 'static'});
                            }
                        });


                        //Replace widget ad if is the content too high!
                        var sssp_max_document_height = 3000;
                        var sssp_time_delay = 6000;
                        if (ssspsticky === true && (sssp_flag_one === true || sssp_flag_two === true || sssp_flag_three === true)) {
                            var sssp_widget_element = jQuery('#' + widget_id);
                            var sssp_widget_offset = sssp_widget_element.offset().top;

                            if (sssp_widget_offset > sssp_max_document_height - adOffset &&
                                sticky_start_time + sssp_time_delay < sticky_current_time &&
                                sssp_flag_one === true) {
                                jQuery.when(sssp_widget_element.removeAttr('style').find('.sssp-posCont').remove()).then(sssp.getAds(widget_object));
                                sssp_flag_one = false;
                                sssp_flag_two = true;
                                sticky_start_time = jQuery.now();
                            } else if (
                                sssp_widget_offset > sssp_max_document_height * 2 - adOffset &&
                                sticky_start_time + sssp_time_delay + 6000 < sticky_current_time &&
                                sssp_flag_two === true &&
                                sssp_flag_one === false) {
                                jQuery.when(sssp_widget_element.removeAttr('style').find('.sssp-posCont').remove()).then(sssp.getAds(widget_object));
                                sssp_flag_two = false;
                                sssp_flag_three = true;
                                sticky_start_time = jQuery.now();
                            } else if (
                                sssp_widget_offset > sssp_max_document_height * 3 - adOffset &&
                                sticky_start_time + sssp_time_delay + 6000 < sticky_current_time &&
                                sssp_flag_three === true &&
                                sssp_flag_two === false) {
                                jQuery.when(sssp_widget_element.removeAttr('style').find('.sssp-posCont').remove()).then(sssp.getAds(widget_object));
                                sssp_flag_three = false;
                                sticky_start_time = jQuery.now();
                            }
                        }
                    });
                }

            }

            /**
             * Check if the end element is in the viewport
             * @param element
             * @returns {boolean}
             */
            function sssp_isInViewport(element) {
                var el = document.querySelector(element);
                var rect = el.getBoundingClientRect();
                return (
                    rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
                );
            }
        }
    }

});