jQuery(function ($) {
    'use strict';

    var ssspsticky = false;

    //Enable position form field if necessary
    jQuery('#sssp-zone-insert').change(function () {
        sssp_check_insert_field();
        sssp_check_selected_option('sssp-zone-position');
    });
    //Control form on document ready
    jQuery(document).ready(function () {
        sssp_check_insert_field();
        sssp_verify_zone_id();
        sssp_check_selected_option('sssp-zone-position');
        sssp_check_popup_type_relations();
    });
    //Process on position change
    jQuery('#sssp-zone-position').change(function () {
        var position_value = jQuery(this).val();

        if (position_value === 'inarticle') {
            sssp_enable_element(['sssp-zone-inarticle-placement', 'sssp-zone-inarticle-placement-repeat']);
        } else {
            sssp_disable_element(['sssp-zone-inarticle-placement', 'sssp-zone-inarticle-placement-repeat']);
        }
        if (position_value === 'popup') {
            sssp_disable_element(['sssp-ad-alignment', 'sssp-ad-custom-css', 'sssp-ad-margin-top', 'sssp-ad-margin-bottom']);
        } else {
            sssp_enable_element(['sssp-ad-alignment', 'sssp-ad-custom-css', 'sssp-ad-margin-top', 'sssp-ad-margin-bottom']);
        }
        if (position_value === 'headerb') {
            sssp_disable_element(['sssp-ad-type']);
        } else {
            sssp_enable_element(['sssp-ad-type']);
        }
        if (position_value === 'below_a' || position_value === 'inarticle' || position_value === 'popup') {
            sssp_show_allowed_post_types();
        } else {
            sssp_hide_allowed_post_types();
        }

        sssp_switch_preview_image(position_value);

    });


    jQuery('#sssp-ad-type').change(function () {
        sssp_check_popup_type_relations();
    });

    //Zone ID verification
    jQuery('#sssp-zone-id').change(function () {
        sssp_verify_zone_id();
    });

    //Check popup position and desktop and newsfeed relations

    function sssp_check_popup_type_relations() {
        var type_value = jQuery('#sssp-ad-type').val();
        var position_value = jQuery('#sssp-zone-position').val();
        if (type_value === 'desktop' && position_value === 'popup') {
            jQuery('#sssp-ad-newsfeed').prop("checked", false);
            sssp_disable_input(['sssp-ad-newsfeed-1']);
            sssp_select_input(['sssp-ad-newsfeed-0']);
        } else {
            sssp_enable_input(['sssp-ad-newsfeed-1']);
        }
    }

    /**
     * Check insert fields function
     */
    function sssp_check_insert_field() {
        if (jQuery('#sssp-zone-insert').val() === 'automatic') {
            sssp_disable_position_options(['widget', 'other']);
            sssp_enable_position_options(['popup', 'inarticle']);
            sssp_hide_box(['sssp-insert-box']);
        } else {
            sssp_disable_element(['sssp-zone-inarticle-placement', 'sssp-zone-inarticle-placement-repeat']);
            sssp_enable_position_options(['widget', 'other']);
            sssp_disable_position_options(['popup', 'inarticle']);
            sssp_show_box(['sssp-insert-box']);
        }
    }

    /**
     * Hide box - Input array
     * @param elements
     */
    function sssp_hide_box(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).hide(0);
        })
    }

    /**
     * Show box - Input array
     * @param elements
     */
    function sssp_show_box(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).show(0);
        })
    }

    /**
     * Disable elements if necessary - Input array
     * @param elements
     */
    function sssp_disable_element(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).prop('disabled', 'disabled');
        })
    }

    /**
     * Enable elements if necessary - Input array
     * @param elements
     */
    function sssp_enable_element(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).prop('disabled', false);
        })
    }

    /**
     * This function enable options in position select if necessary
     * @param elements
     */
    function sssp_enable_position_options(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#sssp-zone-position option[value="' + value + '"]').prop('disabled', false);
        })
    }

    /**
     * This function disable options in position select if necessary
     * @param elements
     */
    function sssp_disable_position_options(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#sssp-zone-position option[value="' + value + '"]').prop('disabled', 'disabled');
        })
    }

    /**
     * This function disable inputs
     * @param elements
     */
    function sssp_disable_input(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).prop('disabled', 'disabled');
        })
    }

    /**
     * This function enable inputs
     * @param elements
     */
    function sssp_enable_input(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).prop('disabled', false);
        })
    }

    /**
     * This function select input if necessary
     * @param elements
     */
    function sssp_select_input(elements) {
        jQuery.each(elements, function (index, value) {
            jQuery('#' + value).prop('checked', true);
        })
    }


    /**
     * This function is checking if is selected disabled option and if yes it select first value instead
     * @param element_id
     */
    function sssp_check_selected_option(element_id) {
        var selector_main = '#' + element_id;
        if (jQuery(selector_main).find(':selected').prop('disabled')) {
            jQuery(selector_main).val(jQuery(selector_main + ' option:first').val());
        }
        if (jQuery(selector_main).val() !== 'popup') {
            sssp_enable_element(['sssp-ad-alignment', 'sssp-ad-custom-css', 'sssp-ad-margin-top', 'sssp-ad-margin-bottom']);
        } else {
            sssp_disable_element(['sssp-ad-alignment', 'sssp-ad-custom-css', 'sssp-ad-margin-top', 'sssp-ad-margin-bottom']);
        }
        if (jQuery(selector_main).val() === 'headerb') {
            sssp_disable_element(['sssp-ad-type']);
        } else {
            sssp_enable_element(['sssp-ad-type']);
        }
        if (jQuery(selector_main).val() === 'inarticle') {
            sssp_enable_element(['sssp-zone-inarticle-placement', 'sssp-zone-inarticle-placement-repeat']);
        }
        if (jQuery(selector_main).val() === 'inarticle' || jQuery(selector_main).val() === 'below_a' || jQuery(selector_main).val() === 'popup') {
            sssp_show_allowed_post_types();
        } else {
            sssp_hide_allowed_post_types();
        }
    }

    /**
     * Function for verifying Zone ID
     */
    function sssp_verify_zone_id() {

        if (!jQuery('#sssp-zone-id').val()) {
            return;
        }

        const zoneId = jQuery('#sssp-zone-id').val();
        const ver_url = 'https://ssp.seznam.cz/v1/xhr';
        var data = {
            "site": "seznam.cz",
            "bhash": 1234567890,
            "pvId": "123456",
            "zones": [
                {
                    "zoneId": zoneId,
                    "width": 4000
                }
            ]
        };

        jQuery.ajax({
            type: "POST",
            dataType: 'json',
            url: ver_url,
            data: JSON.stringify(data),
            processData: false,
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, jQxhr) {
                jQuery('#sssp-zone-id-error').text('#').css('visibility', 'hidden');
                var response_text = jQuery('#sssp-data-holder').data('hidden-message');
                jQuery('#sssp-zone-id-message').val(response_text);
                jQuery('#sssp-zone-id').css('border', '1px solid #C0C0C0');
            },
            error: function (jqXhr, textStatus, errorThrown) {
                var response = jqXhr.responseJSON.error;
                if (response !== 'undefined') {
                    var response_text = jQuery('#sssp-data-holder').data('error')
                    jQuery('#sssp-zone-id-error').text(response_text).css('visibility', 'visible');
                    jQuery('#sssp-zone-id-message').val(response_text);
                }
                jQuery('#sssp-zone-id').css('border', '1px solid #CC0000');
                console.log(errorThrown)
            }
        });
    }

    /**
     * Change preview image
     * @param position_value
     */
    function sssp_switch_preview_image(position_value) {
        if (position_value === 'headerb') {
            jQuery('.sssp-img-withoutbranding').slideUp(0);
            jQuery('.sssp-img-withbranding').slideDown(0);
        } else {
            jQuery('.sssp-img-withoutbranding').slideDown(0);
            jQuery('.sssp-img-withbranding').slideUp(0);
        }
    }

    /**
     * Show allowed post types for article ads
     */
    function sssp_show_allowed_post_types() {
        jQuery('#sssp-allowed-post-types').show(0)
    }

    /**
     * Show allowed post types for article ads
     */
    function sssp_hide_allowed_post_types() {
        jQuery('#sssp-allowed-post-types').hide(0)
    }

    /**
     * Sticky element "HELP"
     * @param breakPoint
     * @param onScrollElementId
     */
    function sssp_sticky(breakPoint, onScrollElementId) {
        if (window.innerWidth >= 1890) {
            jQuery(window).scroll(function () {
                var top = jQuery(window).scrollTop();
                var offset = jQuery('#' + onScrollElementId).offset();

                if (offset) {
                    if (jQuery('.' + breakPoint).offset().top < top) {
                        jQuery('#' + onScrollElementId).addClass('sssp-sticky').css({'left': offset.left});
                        ssspsticky = true;
                    } else {
                        jQuery('#' + onScrollElementId).removeClass('sssp-sticky');
                        ssspsticky = false;
                    }
                }
            });
        }
    }

    jQuery(document).ready(function () {
        sssp_sticky('sssp-post-table', 'sssp-post-table-right');
    });

    //Remove sticky if is resized window
    jQuery(window).resize(function () {
        if (ssspsticky === true) {
            jQuery('#sssp-post-table-right').removeClass('sssp-sticky');
        }
    });


    /* Admin Area
    *
    *
    *
    * */

    jQuery('#sssp-allowed-capability').change(function () {
        sssp_check_capability_option();
    });

    jQuery('#sssp-allowed-capability-special').change(function () {
        sssp_set_spec_capability_value();
    });

    //Control form on document ready
    jQuery(document).ready(function () {
        sssp_check_capability_option();
    });

    /**
     * Check capability option
     */
    function sssp_check_capability_option() {

        if (jQuery('#sssp-allowed-capability').find('option').eq(3).is(':selected')) {
            sssp_enable_element(['sssp-allowed-capability-special']);
        } else {
            sssp_disable_element(['sssp-allowed-capability-special']);
        }
    }

    /**
     * Always synchronize selected special capability with value on the option in limit access option.
     */
    function sssp_set_spec_capability_value() {
        let new_value = jQuery('#sssp-allowed-capability-special').val();
        jQuery('#sssp-allowed-capability').find('option').eq(3).val(new_value);
    }

    jQuery(document).ready(function ($) {
        if (document.getElementById("sssp-branding-css") !== null) {
            wp.codeEditor.initialize(jQuery('#sssp-branding-css'), cm_settings);
        }
        if (document.getElementById("sssp-popup-css") !== null) {
            wp.codeEditor.initialize(jQuery('#sssp-popup-css'), cm_settings);
        }
    })

    // Copy Code
    jQuery(document).on("click", '#sssp-copy-code', function (e) {
        e.preventDefault();
        var sssp_code_target = jQuery(this).parent().data('target');
        var sssp_textarea_to_copy = jQuery('.sssp-textarea-preview-' + sssp_code_target);

        jQuery(sssp_textarea_to_copy).select();
        document.execCommand("copy");
    });

});
