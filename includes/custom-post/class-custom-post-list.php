<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_CustomPostList
{
    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Constructor
     *
     * @since     1.0.0
     */

    public function __construct()
    {
        //Add actions
        add_action('manage_' . SEZNAM_SSP_POST_TYPE_SLUG . '_posts_custom_column', [$this, 'organize_item_columns'], 10, 2);

        //Filters
        add_filter('manage_' . SEZNAM_SSP_POST_TYPE_SLUG . '_posts_columns', [$this, 'add_sticky_column']);
        add_filter('bulk_actions-edit-' . SEZNAM_SSP_POST_TYPE_SLUG, [$this, 'customize_bulk_actions'], 10, 1);
        add_filter('post_row_actions', [$this, 'remove_quick_edit'], 10, 2);

        //AD filters
        add_action('restrict_manage_posts', [$this, 'list_admin_posts_filter_restrict_manage_posts']);
        add_filter('parse_query', [$this, 'list_posts_filter']);

        //Bulk actions
        add_filter('handle_bulk_actions-edit-' . SEZNAM_SSP_POST_TYPE_SLUG, [$this, 'gpc_bulk_action_handler'], 10, 3);


    }

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @param $column
     * @param $post_id
     */
    public function organize_item_columns($column, $post_id)
    {

        $ad_meta = SSSP_Main::get_ad_meta($post_id);

        // Shortcode column
        if ('sssp-shortcode' === $column) {
            if ($ad_meta['sssp-zone-position'] != 'popup' && $ad_meta['sssp-zone-position'] != 'inarticle') {
                echo '[' . SEZNAM_SSP_SLUG . ' id="' . $post_id . '"]';
            } else {
                echo html_entity_decode(esc_html__('This type of ad cannot<br>be displayed using a shortcode!', SEZNAM_SSP_SLUG));
            }

        }
        // Status column
        if ('sssp-status' === $column) {
            if (isset($ad_meta['sssp-ad-active'])) {
                switch ($ad_meta['sssp-ad-active']) {
                    case 'active':
                        echo '<div class="sssp-bullet-box"><div class="sssp-bullet">' . esc_attr__('Active', SEZNAM_SSP_SLUG) . '</div></div>';
                        break;
                    case 'notactive':
                        echo '<div class="sssp-bullet-box"><div class="sssp-bullet-red">' . esc_attr__('Not active', SEZNAM_SSP_SLUG) . '</div></div>';
                        break;
                    case 'wait':
                        echo '<div class="sssp-bullet-box"><div class="sssp-bullet-black">' . esc_attr__('Waiting for approval', SEZNAM_SSP_SLUG) . '</div></div>';
                        break;
                    case 'indev':
                        echo '<div class="sssp-bullet-box"><div class="sssp-bullet-blue">' . esc_attr__('Development mode', SEZNAM_SSP_SLUG) . '</div></div>';
                        break;
                }
                $newsfeed = get_post_meta($post_id, 'sssp-ad-newsfeed', true);
                if ($newsfeed && $newsfeed == '1') {
                    echo '<br><span class="sssp-seznam-blue">' . esc_attr__('For Seznam Newsfeed visitors only', SEZNAM_SSP_SLUG) . '</span>';
                }
            }
        }
        //Positions
        if ('sssp-position' === $column) {
            if (isset($ad_meta['sssp-zone-position'])) {
                $current_position = $ad_meta['sssp-zone-position'];
                $standard_positions = SSSP_CustomPostEdit::get_standard_ad_positions();
                $options = [];
                foreach ($standard_positions as $s_key => $s_value) {
                    $options[$s_key] = $s_value['name'];
                }
                foreach ($options as $pos_key => $pos_value) {
                    if ($current_position == $pos_key) {
                        echo $pos_value;
                    }
                }
            }

        }
        //Zone ID
        if ('sssp-zoneid' === $column) {
            if (isset($ad_meta['sssp-zone-id'])) {
                echo $ad_meta['sssp-zone-id'] . '<br>';
                $message = get_post_meta($post_id, 'sssp-zone-id-message', true);
                $message_default = esc_attr__('Zone ID is verified', SEZNAM_SSP_SLUG);
                if ($message == $message_default) {
                    echo $message;
                } else {
                    echo '<span class="sssp-seznam-red">' . $message . '</span>';
                }


            }
        }

        //Type
        if ('sssp-type' === $column) {
            if (isset($ad_meta['sssp-ad-type'])) {
                switch ($ad_meta['sssp-ad-type']) {

                    case 'desktop':
                        echo esc_attr__('Desktop', SEZNAM_SSP_SLUG) . '<br>';
                        break;
                    case 'mobile':
                    default:
                        echo esc_attr__('Mobile devices', SEZNAM_SSP_SLUG) . '<br>';
                        break;
                }
                echo $ad_meta['sssp-zone-width'] . 'x' . $ad_meta['sssp-zone-height'] . '<br>';
            }
        }

        //Inserted
        if ('sssp-inserted' === $column) {
            if (isset($ad_meta['sssp-zone-insert'])) {
                $inserted = $ad_meta['sssp-zone-insert'];

                switch ($inserted) {
                    case 'manual':
                        echo esc_attr__('Manual', SEZNAM_SSP_SLUG);
                        break;

                    case 'automatic':
                        echo esc_attr__('Automatically', SEZNAM_SSP_SLUG);
                        break;
                }
            }
        }

    }

    /**
     * @param $columns
     * @return array
     */
    public function add_sticky_column($columns)
    {
        $columns = [
            'cb' => $columns['cb'],
            'title' => esc_attr__('Ad title', SEZNAM_SSP_SLUG),
            'sssp-zoneid' => esc_attr__('Zone ID', SEZNAM_SSP_SLUG),
            'sssp-status' => esc_attr__('Status', SEZNAM_SSP_SLUG),
            'sssp-position' => esc_attr__('Position', SEZNAM_SSP_SLUG),
            'sssp-inserted' => esc_attr__('Inserted', SEZNAM_SSP_SLUG),
            'sssp-type' => esc_attr__('Type', SEZNAM_SSP_SLUG),
            'sssp-shortcode' => esc_attr__('Shortcode', SEZNAM_SSP_SLUG),
        ];
        return $columns;
    }

    /**
     * @param $actions
     * @return mixed
     * Remove bulk actions view or quick-edit
     */
    public function customize_bulk_actions($actions)
    {
        unset($actions['edit']);
        unset($actions['inline']);

        $actions['sssp-filter-active'] = esc_attr__('Set to Active', SEZNAM_SSP_SLUG);
        $actions['sssp-filter-notactive'] = esc_attr__('Set to Not active', SEZNAM_SSP_SLUG);
        $actions['sssp-filter-indev'] = esc_attr__('Set to Development mode', SEZNAM_SSP_SLUG);
        $actions['sssp-filter-wait'] = esc_attr__('Set to Waiting for approval', SEZNAM_SSP_SLUG);

        return $actions;
    }

    /**
     * @param $actions
     * @param $post
     * @return mixed|void
     */
    public function remove_quick_edit($actions, $post)
    {
        if ($post->post_type == SEZNAM_SSP_POST_TYPE_SLUG) {
            unset($actions['view']);
            unset($actions['inline hide-if-no-js']);
            return $actions;
        }
        return $actions;
    }

    /**
     * First create the dropdown
     * make sure to change POST_TYPE to the name of your custom post type
     *
     * @return void
     * @author Ohad Raz
     *
     */
    public function list_admin_posts_filter_restrict_manage_posts()
    {
        $type = SEZNAM_SSP_POST_TYPE_SLUG;
        if (isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
        }

        if (isset($_GET['post_type']) && $_GET['post_type'] == $type) {

            //Special filter for post type
            ?>

            <select name="filter_by_type">
                <option value=""><?php esc_attr_e('— Filter by status —', SEZNAM_SSP_SLUG); ?></option>
                <option value="active"><?php esc_attr_e('Active', SEZNAM_SSP_SLUG); ?></option>
                <option value="notactive"><?php esc_attr_e('Not active', SEZNAM_SSP_SLUG); ?></option>
                <option value="wait"><?php esc_attr_e('Waiting for approval', SEZNAM_SSP_SLUG); ?></option>
                <option value="indev"><?php esc_attr_e('Development mode', SEZNAM_SSP_SLUG); ?></option>
            </select>

            <select name="filter_by_position">
                <option value=""><?php esc_attr_e('— Filter by position —', SEZNAM_SSP_SLUG); ?></option>
                <?php
                $standard_positions = SSSP_CustomPostEdit::get_standard_ad_positions();
                foreach ($standard_positions as $key => $value)
                    echo '<option value="' . $key . '">' . $value['name'] . '</option>'
                ?>
            </select>

            <?php

            //Authors filter
            if (isset($_GET['filter_by_zone_id'])) {
                $f_ip = $_GET['filter_by_zone_id'];
            } else {
                $f_ip = '';
            }

            ?>
            <input name="filter_by_zone_id" type="text" id="sssp-filter_by_zone_id"
                   placeholder="<?php esc_attr_e('— Filter by Zone ID —', SEZNAM_SSP_SLUG); ?>" value="<?php echo $f_ip; ?>">

            <input type="reset" name="filter_action" id="post-query-submit" class="button" value="<?php esc_attr_e('Reset filter', SEZNAM_SSP_SLUG); ?>">
            <?php
        }
    }

    /**
     * if submitted filter by post meta
     *
     * make sure to change META_KEY to the actual meta key
     * and POST_TYPE to the name of your custom post type
     * @param  (wp_query object) $query
     *
     * @return Void
     * @author Ohad Raz
     */
    public function list_posts_filter($query)
    {
        global $pagenow;
        $type = SEZNAM_SSP_POST_TYPE_SLUG;
        if (isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
        }

        $argument = [];


        //Type filter
        if (isset($_GET['post_type']) && $_GET['post_type'] == $type && is_admin() && $pagenow == 'edit.php' && isset($_GET['filter_by_type']) && $_GET['filter_by_type'] != '') {
            $argument[] = [
                'key' => 'sssp-ad-active',
                'value' => $_GET['filter_by_type'],
            ];
        } else {
            $argument[] = [null];
        }

        //Position filter
        if (isset($_GET['post_type']) && $_GET['post_type'] == $type && is_admin() && $pagenow == 'edit.php' && isset($_GET['filter_by_position']) && $_GET['filter_by_position'] != '') {
            $argument[] = [
                'key' => 'sssp-zone-position',
                'value' => $_GET['filter_by_position'],
            ];
        } else {
            $argument[] = [null];
        }

        //Position filter
        if (isset($_GET['post_type']) && $_GET['post_type'] == $type && is_admin() && $pagenow == 'edit.php' && isset($_GET['filter_by_zone_id']) && $_GET['filter_by_zone_id'] != '') {
            $argument[] = [
                'key' => 'sssp-zone-id',
                'value' => $_GET['filter_by_zone_id'],
            ];
        } else {
            $argument[] = [null];
        }

        $arguments = [$argument];
        $query->query_vars['meta_query'] = $arguments;

    }

    /*
     * Define what to do with bulk actions
     */
    public function gpc_bulk_action_handler($redirect, $doaction, $object_ids)
    {

        // Filter by status
        $list_of_active_actions = ['active', 'notactive', 'indev', 'wait'];
        foreach ($list_of_active_actions as $action) {
            if ($doaction == 'sssp-filter-' . $action) {
                foreach ($object_ids as $post_id) {
                    $ad_meta = SSSP_Main::get_ad_meta($post_id);
                    //Just change the status
                    update_post_meta($post_id, 'sssp-ad-active', $action);
                    if ($ad_meta) {
                        $ad_meta['sssp-ad-active'] = $action;
                        update_post_meta($post_id, 'sssp_ad_meta', base64_encode(serialize($ad_meta)));
                    }
                }
                $redirect = add_query_arg('gpc_bulk_approved', count($object_ids), $redirect);
            }
        }

        return $redirect;
    }


}