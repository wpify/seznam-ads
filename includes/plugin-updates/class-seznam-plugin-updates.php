<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_PluginUpdates
{

    /**
     * @var string
     */
    public $plugin_slug;
    /**
     * @var string
     */
    public $version;
    /**
     * @var string
     */
    public $cache_key;
    /**
     * @var bool
     */
    public $cache_allowed;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->plugin_slug = SEZNAM_SSP_SLUG;
        $this->version = SEZNAM_SSP_VERSION;
        $this->cache_key = SEZNAM_SSP_SLUG . '-upd';
        $this->cache_allowed = true;

        add_filter('plugins_api', [$this, 'info'], 20, 3);
        add_filter('site_transient_update_plugins', [$this, 'update']);
        add_action('upgrader_process_complete', [$this, 'purge'], 10, 2);

    }

    /**
     * @return false|mixed|null
     */
    public function request()
    {

        $remote = get_transient($this->cache_key);

        if( false === $remote || ! $this->cache_allowed ) {

        $remote = wp_remote_get(
            SEZNAM_SSP_UPDATE_JSON,
            [
                'sslverify' => false,
                'timeout' => 10,
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );

        if (
            is_wp_error($remote)
            || 200 !== wp_remote_retrieve_response_code($remote)
            || empty(wp_remote_retrieve_body($remote))
        ) {
            return false;
        }

        set_transient($this->cache_key, $remote, DAY_IN_SECONDS);

        }

        $jsondata = json_decode(wp_remote_retrieve_body($remote));

        if(isset($jsondata->seznam_ads)){
            return $jsondata->seznam_ads;
        }
    }


    /**
     * @param $res
     * @param $action
     * @param $args
     * @return false|stdClass
     */
    public function info($res, $action, $args)
    {
        // do nothing if you're not getting plugin information right now
        if ('plugin_information' !== $action) {
            return false;
        }

        // do nothing if it is not our plugin
        if ($this->plugin_slug !== $args->slug) {
            return false;
        }

        // get updates
        $remote = $this->request();

        if (!$remote) {
            return false;
        }

        $res = new stdClass();

        $res->name = $remote->name;
        $res->slug = $remote->slug;
        $res->version = $remote->version;
        $res->tested = $remote->tested;
        $res->requires = $remote->requires;
        $res->author = $remote->author;
        $res->author_profile = 'https://www.seznam.cz';
        $res->download_link = $remote->download_url;
        $res->trunk = $remote->download_url;
        $res->requires_php = $remote->requires_php;
        $res->last_updated = $remote->last_updated;

        $res->sections = [
            'description' => $remote->sections->description,
            'installation' => $remote->sections->installation,
            'changelog' => $remote->sections->changelog,
        ];

        if (!empty($remote->banners)) {
            $res->banners = [
                'low' => $remote->banners->low,
                'high' => $remote->banners->high,
            ];
        }

        return $res;

    }

    /**
     * @param $transient
     * @return mixed,
     */
    public function update($transient)
    {

        if (empty($transient->checked)) {
            return $transient;
        }

        $remote = $this->request();

        if (
            $remote
            && version_compare($this->version, $remote->version, '<')
            && version_compare($remote->requires, get_bloginfo('version'), '<')
            && version_compare($remote->requires_php, PHP_VERSION, '<')
        ) {
            $res = new stdClass();
            $res->slug = $this->plugin_slug;
            $res->plugin = SEZNAM_SSP_SLUG.'/seznam-ssp.php'; // seznam-ads/seznam-ssp.php
            $res->new_version = $remote->version;
            $res->tested = $remote->tested;
            $res->package = $remote->download_url;

            $transient->response[$res->plugin] = $res;

        }

        return $transient;

    }

    /**
     * @param $upgrader_object
     * @param $options
     */
    public function purge($upgrader_object, $options)
    {
        if (
            $this->cache_allowed
            && 'update' === $options['action']
            && 'plugin' === $options['type']
        ) {
            // just clean the cache when new plugin version is installed
            delete_transient($this->cache_key);
        }

    }


}