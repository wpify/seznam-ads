<?php

if (!defined('WPINC')) {
    wp_die();
}

require_once(SEZNAM_SSP_PATH . 'shortcodes/class-seznam-ads-shortcode.php'); //GP Contest Shortcode
require_once(SEZNAM_SSP_PATH . 'shortcodes/class-seznam-ssp-shortcode.php'); //GP Contest Shortcode


add_action('plugins_loaded', ['SSSP_Shortcode', 'get_instance']); //Load Contest Shortcode
add_action('plugins_loaded', ['SSSP_Shortcode_SSP', 'get_instance']); //Load Contest Shortcode