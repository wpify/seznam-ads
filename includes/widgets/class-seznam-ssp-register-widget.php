<?php

if (!defined('WPINC')) {
    wp_die();
}

// Creating the widget
class SSSP_RegisterWidget extends WP_Widget
{

    /**
     *
     */
    function __construct()
    {
        parent::__construct(

        // Base ID of your widget
            SEZNAM_SSP_WIDGET_SLUG,

            // Widget name will appear in UI
            esc_attr__('Seznam Reklama WP widget', SEZNAM_SSP_SLUG),

            // Widget description
            ['description' => esc_attr__('This widget allows you to put created Seznam ads to your content', SEZNAM_SSP_SLUG),]
        );
    }

    /**
     * Creating widget front-end
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        //Add sticky class if selected
        if (isset($instance['sticky']) && $instance['sticky'] == 1) {
            $args['before_widget'] = str_replace('class="', 'class="sssp-sticky-box ', $args['before_widget']);
        }

        //Get type mobile/desktop
        $ad_meta = SSSP_Main::get_ad_meta($instance['ad_id']);

        //Check the newsfeed option
        if(isset($ad_meta['sssp-ad-newsfeed'])) {
            $sssp_newsfeed = SSSP_Main::check_ssp_newsfeed();
            if ($ad_meta['sssp-ad-newsfeed'] != $sssp_newsfeed) {
                return;
            }
        }

        if ($ad_meta) {
            switch ($ad_meta['sssp-ad-active']) {
                case 'active':
                    switch ($ad_meta['sssp-ad-type']) {
                        case 'desktop':
                            $args['before_widget'] = str_replace('class="', 'class="sssp-seznam-ad-desktop ', $args['before_widget']);
                            break;
                        case 'mobile':
                            $args['before_widget'] = str_replace('class="', 'class="sssp-seznam-ad-mobile ', $args['before_widget']);
                            break;
                    }
                    break;
                case 'indev':
                    if(current_user_can(SEZNAM_SSP_CAPABILITY)){
                        switch ($ad_meta['sssp-ad-type']) {
                            case 'desktop':
                                $args['before_widget'] = str_replace('class="', 'class="sssp-seznam-ad-desktop ', $args['before_widget']);
                                break;
                            case 'mobile':
                                $args['before_widget'] = str_replace('class="', 'class="sssp-seznam-ad-mobile ', $args['before_widget']);
                                break;
                        }
                    }else{
                        return;
                    }
                    break;
                default:
                    return;
            }

        }

        if (isset($instance['sticky']) && $instance['sticky'] == 1) {
            echo '<div id="sssp-breakpoint" style="visibility: hidden"></div>';
        }

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        //Add sticky class if selected
        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        if (isset($instance['ad_id']) && is_numeric(($instance['ad_id']))) {
            $verify_ad = $this->verify_ad($instance['ad_id']);
            if ($verify_ad) {
                echo do_shortcode('[seznam-ads id="' . $instance['ad_id'] . '"]');

            }
        }
        echo '<div style="clear: both"></div>';
        echo $args['after_widget'];
    }

    /**
     * @param array $instance
     * @return void
     */
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
        if (!isset($instance['sticky'])) {
            $instance['sticky'] = NULL;
        }
        if (!isset($instance['ad_id'])) {
            $instance['ad_id'] = NULL;
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>

        </p>

        <?php
        $ads = $this->get_widgets_ads();

        if ($ads) { ?>
            <p>
                <label for="<?php echo $this->get_field_name('ad_id'); ?>"><?php esc_attr_e('Select Ad to display in the widget', SEZNAM_SSP_SLUG); ?></label>
                <select
                        id="<?php echo $this->get_field_id('ad_id'); ?>"
                        name="<?php echo $this->get_field_name('ad_id'); ?>"
                        style="width: 100%; box-sizing: border-box">
                    <option value="0">— <?php esc_attr_e('Select Ad', SEZNAM_SSP_SLUG); ?> —</option>

                    <?php foreach ($ads as $ad): ?>
                        <option <?php selected($ad->ID, $instance['ad_id'], TRUE); ?>
                                value="<?php echo $ad->ID; ?>"><?php echo $ad->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </p>

            <?php
        }

        ?>
        <p>
            <input class="checkbox" type="checkbox" <?php checked($instance['sticky'], '1', TRUE) ?>
                   id="<?php echo $this->get_field_id('sticky'); ?>"
                   name="<?php echo $this->get_field_name('sticky'); ?>"
                   value="<?php echo $instance['sticky'] ?>>"
            >
            <label for="<?php echo $this->get_field_name('sticky'); ?>"><?php esc_attr_e('Make widget sticky', SEZNAM_SSP_SLUG); ?> *</label>
        </p>
        <p>* <?php esc_attr_e('Widget will be sticky only if ist the last one in the sidebar!', SEZNAM_SSP_SLUG); ?></p>
        <?php
    }

    /**
     * @return false|int[]|WP_Post[]
     */
    private function get_widgets_ads()
    {

        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'meta_key' => 'sssp-zone-position',
            'meta_value' => 'widget',
        ];
        $ads = get_posts($args);

        if ($ads) {
            return $ads;
        }

        return false;

    }

    /**
     * Updating widget replacing old instances with new
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['ad_id'] = (!empty($new_instance['ad_id'])) ? strip_tags($new_instance['ad_id']) : '';
        $instance['sticky'] = (isset($new_instance['sticky'])) ? '1' : '';
        return $instance;
    }

    /**
     * @param $item_id
     * @return bool
     */
    private function verify_ad($item_id)
    {
        if (FALSE === get_post_status($item_id)) {
            return false;
        }
        if (get_post_meta($item_id, 'sssp-zone-position', true) != 'widget') {
            return false;
        }

        return true;

    }
}