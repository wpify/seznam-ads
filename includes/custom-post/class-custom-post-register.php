<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_CustomPost
{

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Constructor
     *
     * @since     1.0.0
     */

    public function __construct()
    {
        add_action('init', [$this, 'post_register']);
        add_action('admin_menu', [$this, 'add_menu_page']);
    }

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @return void
     */
    public function post_register()
    {
        $labels = [
            'name' => esc_attr__('Ads', SEZNAM_SSP_SLUG),
            'menu_name' => esc_attr__('Ads', SEZNAM_SSP_SLUG),
            'all_items' => esc_attr__('Ads', SEZNAM_SSP_SLUG),
            'singular_name' => esc_attr__('Ad', SEZNAM_SSP_SLUG),
            'add_new' => esc_attr__('Add new Ad', SEZNAM_SSP_SLUG),
            'add_new_item' => esc_attr__('Create new Ad', SEZNAM_SSP_SLUG),
            'edit_item' => esc_attr__('Edit Ad', SEZNAM_SSP_SLUG),
            'new_item' => esc_attr__('New Ad', SEZNAM_SSP_SLUG),
            'view_item' => esc_attr__('View Ad', SEZNAM_SSP_SLUG),
            'search_items' => esc_attr__('Search Ad', SEZNAM_SSP_SLUG),
            'not_found' => esc_attr__('Nothing found', SEZNAM_SSP_SLUG),
            'not_found_in_trash' => esc_attr__('Nothing found in Trash', SEZNAM_SSP_SLUG),
            'parent_item_colon' => '',
        ];

        $args = [
            'labels' => $labels,
            'public' => false,
            'publicly_queryable' => false,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => [
                'slug' => SEZNAM_SSP_POST_TYPE_SLUG,
                'with_front' => true,
            ],
            'capability_type' => 'post',
            'capabilities' => [
                'publish_posts' => SEZNAM_SSP_CAPABILITY,
                'edit_others_posts' => SEZNAM_SSP_CAPABILITY,
                'delete_posts' => SEZNAM_SSP_CAPABILITY,
                'delete_others_posts' => SEZNAM_SSP_CAPABILITY,
                'read_private_posts' => SEZNAM_SSP_CAPABILITY,
                'edit_post' => SEZNAM_SSP_CAPABILITY,
                'delete_post' => SEZNAM_SSP_CAPABILITY,
                'read_post' => SEZNAM_SSP_CAPABILITY,
            ],
            'hierarchical' => false,
            'menu_position' => NULL,
            'supports' => ['title'],
            'show_in_menu' => SEZNAM_SSP_SLUG,
            'show_in_nav_menus' => true,
            'has_archive' => false,
            'delete_with_user' => false,
            'exclude_from_search' => true,
        ];

        register_post_type(SEZNAM_SSP_POST_TYPE_SLUG, $args);

    }

    /**
     * @return void
     */
    public function add_menu_page()
    {

        $icon_base64 = 'PHN2ZyBjbGFzcz0idy02IGgtNiIgaWQ9IkxheWVyXzEiIGRhdGEtbmFtZT0iTGF5ZXIgMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMjQgMjQiPjxwYXRoIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZCIgZD0iTTEwLjg0LDUuMTNjLS4yOS4wNy0uNTguMTItLjg3LjJhNy42OSw3LjY5LDAsMCwwLS43OS4yMWMtLjI1LjA4LS41LjE2LS43NS4yNmE0Ljc5LDQuNzksMCwwLDAtMS4zMi43OCw0LjI1LDQuMjUsMCwwLDAtLjY1LjY5LDUuNDMsNS40MywwLDAsMC0uMzUuNTcsMS40MSwxLjQxLDAsMCwwLS4xNy42MywyLjE2LDIuMTYsMCwwLDAsLjg2LDEuNThjLjE1LjEzLjMyLjI1LjQ4LjM3YTExLjQ5LDExLjQ5LDAsMCwwLDEuNTYuOWMuNTUuMjcsMS4xMS41MywxLjY4Ljc3czEuMDguNDMsMS42My42MWwuNDguMTZjLjUuMTcsMSwuMzMsMS41MS41MS4zMS4xMS42NC4yMi45NS4zNmE1LjIzLDUuMjMsMCwwLDEsLjkzLjUxLDEuNzksMS43OSwwLDAsMSwuNDguNTEsMS40MiwxLjQyLDAsMCwxLC4yLjY3LDEuMTgsMS4xOCwwLDAsMS0uMTguNjMsMywzLDAsMCwxLTEuMzcsMS4wNSwyMi42OSwyMi42OSwwLDAsMS0yLjM5Ljg2Yy0uNDYuMTQtLjkyLjI4LTEuMzkuNEwxMCwxOC43M2MtLjQ3LjEzLS45NC4yMy0xLjQxLjM0bC0uODIuMTctLjQ4LjEtMSwuMjMtMS4xNi4yNS0uMzMuMDhjLS4xLDAtLjQ4LjExLS4yLjFzMS40Mi0uMTIsMy42My0uNDFjLjc5LS4xNCwxLjYtLjI1LDIuNC0uMzksMS0uMTYsMi0uMzYsMy0uNTlsLjg0LS4yMi41NS0uMTZjLjc3LS4yNSwyLS42NywyLjMxLS44LDEuMTItLjUsMS44Mi0uODgsMi4xOC0xLjc3YTIuMDYsMi4wNiwwLDAsMCwuMTUtMSwyLjQsMi40LDAsMCwwLS4zLS45LDMuMjEsMy4yMSwwLDAsMC0uNTktLjY4LDYuMjgsNi4yOCwwLDAsMC0xLjM1LS45NGMtLjE0LS4wOC0uMjgtLjE0LS40My0uMjFsLS41My0uMjQtLjc3LS4yOWMtLjY3LS4yMy0xLjM2LS40NC0yLS42Ni0uMzktLjEzLS43Ny0uMjctMS4xNC0uNDNhNC42OSw0LjY5LDAsMCwxLS40Ni0uMjEsNS40NSw1LjQ1LDAsMCwxLS41My0uMjljLS4yMS0uMTMtLjU2LS4zNS0uNDYtLjY1cy4zLS4zLjQ5LS4zOEE1LjYsNS42LDAsMCwxLDEyLDguNmMuNTYtLjE1LDEuMTQtLjI1LDEuNy0uMzdsLjUtLjExYTguNDMsOC40MywwLDAsMCwxLjMxLS4zNSwxLjU2LDEuNTYsMCwwLDAsMS0uNzgsMi4wNywyLjA3LDAsMCwwLS4zNC0xLjdsLS4zNS0uNTNjLS4xNy0uMjMtLjM1LS40NS0uNTMtLjY4QS4yMi4yMiwwLDAsMCwxNSw0YTcuMzYsNy4zNiwwLDAsMS0yLjgyLjg2QzExLjcyLDUsMTEuMjgsNSwxMC44NCw1LjEzWiI+PC9wYXRoPjwvc3ZnPg==';
        $icon_data_uri = 'data:image/svg+xml;base64,' . $icon_base64;

        if (current_user_can(SEZNAM_SSP_CAPABILITY)):
            add_menu_page(
                esc_attr__('Seznam Reklama', SEZNAM_SSP_SLUG),
                esc_attr__('Seznam Reklama', SEZNAM_SSP_SLUG),
                SEZNAM_SSP_CAPABILITY,
                SEZNAM_SSP_SLUG,
                [$this, 'ssp_menu_page'],
                $icon_data_uri
            );
            add_submenu_page(
                SEZNAM_SSP_SLUG,
                esc_attr__('General settings', SEZNAM_SSP_SLUG),
                esc_attr__('General settings', SEZNAM_SSP_SLUG),
                SEZNAM_SSP_CAPABILITY,
                'seznam-sssp-admin',
                [$this, 'sssp_settings']
            );
        endif;
    }

    /**
     * @return void
     */
    public function sssp_settings()
    {
        if(isset($_GET['page']) && $_GET['page'] == 'seznam-sssp-admin'){
            require_once(SEZNAM_SSP_PATH . 'includes/custom-post/class-custom-post-admin.php');
            SSSP_CustomPostAdmin::display_admin_page();
        }

    }

}