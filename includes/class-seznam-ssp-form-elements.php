<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_FormElements
{
    public static function get_form_field($field_settings)
    {
        $html = '';

        switch ($field_settings['type']){
            case 'hidden':
            case 'submit':
            case 'button':
            case 'checkbox-array':
                $html .= '';
                break;
            case 'checkbox':
                $html .= '<label style="visibility: hidden">' . $field_settings['label'] . '</label>';
                break;
            default:
                $html .= '<label>' . $field_settings['label'] . '</label>';
                break;

        }


        switch ($field_settings['type']) {
            case 'text':
                $html .= self::get_text($field_settings);
                break;
            case 'select':
                $html .= self::get_select($field_settings);
                break;
            case 'radio':
                $html .= self::get_radio($field_settings);
                break;
            case 'textarea':
                $html .= self::get_textarea($field_settings);
                break;
            case 'number':
                $html .= self::get_number($field_settings);
                break;
            case 'checkbox':
                $html .= self::get_checkbox($field_settings);
                break;
            case 'checkbox-array':
                $html .= self::get_checkbox_array($field_settings);
                break;
            case 'hidden':
                $html .= self::get_hidden($field_settings);
                break;
            case 'submit':
                $html .= self::get_submit($field_settings);
                break;
        }

        if ($field_settings['type'] != 'hidden' &&
            $field_settings['type'] != 'checkbox-array' &&
            $field_settings['type'] != 'radio' &&
            $field_settings['type'] != 'button'
        ) {
            $html .= '<div class="sssp-form-error" id="' . $field_settings['name'] . '-error">#</div>';
        }


        return $html;
    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_text($field_settings)
    {
        return '<input 
        ' . $field_settings['required'] . '
        type="' . $field_settings['type'] . '"
        name="' . $field_settings['name'] . '"
        id="' . $field_settings['name'] . '"
        title="' . $field_settings['label'] . '"
        placeholder="' . $field_settings['label'] . '...."
        value="' . $field_settings['value'] . '"
        class="sssp-input-text">';

    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_textarea($field_settings)
    {
        return '<textarea id="' . $field_settings['name'] . '" name="' . $field_settings['name'] . '" class="sssp-textarea">' . $field_settings['value'] . '</textarea>';

    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_number($field_settings)
    {
        if (isset($field_settings['placeholder']) && !empty($field_settings['placeholder'])) {
            $placeholder = 'placeholder="' . $field_settings['placeholder'] . '"';
            $value = 'value="' . $field_settings['value'] . '"';
        } else {
            $placeholder = '';
            if (!empty($field_settings['value'])) {
                $value = 'value="' . $field_settings['value'] . '"';
            } else {
                if('sssp-zone-inarticle-placement' == $field_settings['name']){
                    if(is_numeric($field_settings['value'])){
                        $value = 'value="' . $field_settings['value'] . '"';
                    }else{
                        $value = 'value="' . $field_settings['default'] . '"';
                    }
                }else{
                    $value = 'value="' . $field_settings['default'] . '"';
                }

            }
        }
        return '<input 
        ' . $field_settings['required'] . '
        ' . $field_settings['status'] . '
        type="' . $field_settings['type'] . '"
        name="' . $field_settings['name'] . '"
        id="' . $field_settings['name'] . '"
        class="sssp-input-text"
        title="' . $field_settings['label'] . '"
        ' . $placeholder . '
        ' . $value . '
        min="' . $field_settings['min'] . '"
        max="' . $field_settings['max'] . '"
        step="' . $field_settings['step'] . '">';

    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_select($field_settings)
    {
        if (empty($field_settings['value'])) {
            $field_settings['value'] = $field_settings['default'];
        }
        $html = '<select
        ' . $field_settings['status'] . '
        name="' . $field_settings['name'] . '"
        id="' . $field_settings['name'] . '"
        title="' . $field_settings['label'] . '"
        >';
        foreach ($field_settings['options'] as $key => $value) {
            $html .= '<option value="' . $key . '" ' . selected($key, $field_settings['value'], false) . '>' . $value . '</option>';
        }
        $html .= '</select>';

        return $html;

    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_radio($field_settings)
    {
        if (empty($field_settings['option'])) {
            $field_settings['option'] = $field_settings['default'];
        }

        $html = '<fieldset>';
        foreach ($field_settings['values'] as $value => $value_name) {
            $html .= '<label title="' . strip_tags($value_name) . '">';
            $html .= '<input type="' . $field_settings['type'] . '" id="' . $field_settings['name'] . '-'.$value.'" name="' . $field_settings['name'] . '" value="' . $value . '" ' . checked($value, $field_settings['option'], false) . ' />';
            $html .= '<span>' . $value_name . '</span>';
            $html .= '</label>';
        }
        $html .= '</label>';
        $html .= '</fieldset>';

        return $html;
    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_hidden($field_settings)
    {
        if (!$field_settings['value']) {
            $field_settings['value'] = $field_settings['default'];
        }
        return '<input
        type="' . $field_settings['type'] . '"
        name="' . $field_settings['name'] . '"
        id="' . $field_settings['name'] . '"
        value="' . $field_settings['value'] . '"
        >';
    }


    /**
     * @param $field_settings
     * @return string
     */
    private static function get_checkbox($field_settings)
    {
        $html = '<div class="sssp-form-checkbox-box">';
        $html .= '<input 
        ' . checked($field_settings['value'], '1', false) . '
        type="' . $field_settings['type'] . '"
        name="' . $field_settings['name'] . '"
        id="' . $field_settings['name'] . '"
        title="' . $field_settings['label'] . '"
        value="' . $field_settings['value'] . '"
        class="sssp-input-text">';
        $html .= '<span>' . $field_settings['label'] . '</span>';
        $html .= '</div>';
        return $html;

    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_checkbox_array($field_settings)
    {
        $html = '<div class="sssp-form-checkbox-box">';

        $meta = $field_settings['value'];
        if (empty($meta)) {
            $meta = array_keys($field_settings['values']); // If empty post meta in the first run just mark all post types as checked
        } else {
            $meta = unserialize($meta);
        }

        foreach ($field_settings['values'] as $v_key => $v_value) {

            $html .= '<div style="width: 100%">';
            $html .= '<input 
        ' . checked(true, in_array($v_key, $meta), false) . '
        type="checkbox"
        name="' . $field_settings['name'] . '[]"
        id="' . $field_settings['name'] . '-' . $v_key . '"
        title="' . $v_value . '"
        value="' . $v_key . '"
        class="sssp-input-text">';
            $html .= '<span>' . $v_value . '</span>';
            $html .= '</div>';
        }
        $html .= '<input type="hidden" name="' . $field_settings['name'] . '[]" value="hidden">';
        $html .= '</div>';
        return $html;

    }

    /**
     * @param $field_settings
     * @return string
     */
    private static function get_submit($field_settings)
    {
        if (isset($field_settings['value']) && !$field_settings['value']) {
            $field_settings['value'] = $field_settings['default'];
        }
        return '<input
        type="' . $field_settings['type'] . '"
        name="' . $field_settings['name'] . '"
        id="' . $field_settings['name'] . '"
        value="' . $field_settings['label'] . '"
        >';
    }

}