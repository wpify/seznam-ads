<?php

/**
 *
 * @package   Seznam SSP plugin
 * @author    Zbynek Hovorka [Seznam.cz, a.s.]
 * @link      https://partner.seznam.cz/
 * @copyright 2021+ Seznam.cz
 *
 * @wordpress-plugin
 * Plugin Name:       Seznam Reklama WP
 * Plugin URI:        https://partner.seznam.cz/wordpress-plugin/
 * Description:       Plugin pro snadnou integraci PPC reklamy od společnosti Seznam.cz, a. s.
 * Version:           1.2
 * Author:            Seznam.cz, a.s.
 * Author URI:        https://partner.seznam.cz/
 * Text Domain:       seznam-ads
 */

// If this file is called directly, abort.

if (!defined('WPINC')) {
    wp_die();
}


//Define basic constants
define('SEZNAM_SSP_PATH', plugin_dir_path(__FILE__));
define('SEZNAM_SSP_URL', plugin_dir_url(__FILE__));
define('SEZNAM_SSP_ADMIN_URL', admin_url());
define('SEZNAM_SSP_DATE_FORMAT', get_option('date_format'));
define('SEZNAM_SSP_TIME_FORMAT', get_option('time_format'));
define('SEZNAM_SSP_SLUG', 'seznam-ads');
define('SEZNAM_SSP_CAPABILITY', get_option('sssp-allowed-capability', 'manage_options'));
define('SEZNAM_SSP_VERSION', '1.2');
define('SEZNAM_SSP_POST_TYPE_SLUG', 'seznam-ads');
define('SEZNAM_SSP_NWSFD_COOKIE', 'sssp_session');
define('SEZNAM_SSP_WIDGET_SLUG', 'sssp_widget');
define('SEZNAM_SSP_UPDATE_JSON', base64_decode('aHR0cHM6Ly9wYXJ0bmVyLnNlem5hbS5jei93cC1jb250ZW50L3VwbG9hZHMvc2V6bmFtLXVwZGF0ZXIuanNvbg=='));



//Require main global classes
require_once(SEZNAM_SSP_PATH . 'includes/class-seznam-ssp.php');
//Load main classes
add_action('plugins_loaded', ['SSSP_Main', 'get_instance']); //Main Class

//Activation hooks
register_activation_hook(__FILE__, ['SSSP_Main', 'activate']);
register_deactivation_hook(__FILE__, ['SSSP_Main', 'deactivate']);

if (is_admin()) {
    //Require admin classes
    require_once(SEZNAM_SSP_PATH . 'includes/class-seznam-ssp-admin.php');
    require_once(SEZNAM_SSP_PATH . 'includes/custom-post/class-custom-post-register.php');

    //Run files only when is needed
    if (SSSP_Admin::check_admin()) {
        require_once(SEZNAM_SSP_PATH . 'includes/custom-post/class-custom-post-list.php');
        require_once(SEZNAM_SSP_PATH . 'includes/custom-post/class-custom-post-edit.php');
        require_once(SEZNAM_SSP_PATH . 'includes/class-seznam-ssp-form-elements.php');
    }

    //Load admin classes
    add_action('plugins_loaded', ['SSSP_Admin', 'get_instance']); //Main Class
    add_action('plugins_loaded', ['SSSP_CustomPost', 'get_instance']); //Main Class


    //Run files only when is needed
    if (SSSP_Admin::check_admin()) {
        add_action('plugins_loaded', ['SSSP_CustomPostList', 'get_instance']); //Main Class
        add_action('plugins_loaded', ['SSSP_CustomPostEdit', 'get_instance']); //Main Class
    }
}

if (!is_admin()) {
    require_once(SEZNAM_SSP_PATH . 'shortcodes/shortcodes.php');
    require_once(SEZNAM_SSP_PATH . 'includes/class-seznam-ssp-automatic-insert.php');
    //Load frontend classes
    add_action('plugins_loaded', ['SSSP_AutomaticInsert', 'get_instance']); //Main Class
}

//Global
require_once(SEZNAM_SSP_PATH . 'includes/widgets/class-seznam-ssp-register-widget.php');

//Check updates
if (is_admin() || defined( 'DOING_CRON' ) && DOING_CRON ) {
    include_once(SEZNAM_SSP_PATH . 'includes/plugin-updates/class-seznam-plugin-updates.php');
    new SSSP_PluginUpdates();
}