<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_Shortcode
{
    protected static $instance = NULL;

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    public function __construct()
    {
        //Create Image Gallery Custom Post
        add_shortcode('seznam-ads', [$this, 'shortcode']);

        //JS to header
        add_action('wp_head', [$this, 'js_to_header']);

    }

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (NULL == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param $atts
     * @param null $content
     * @return string
     */
    public function shortcode($atts, $content = NULL)
    {
        if (isset($atts['id'])) {
            $post_id = $atts['id'];
        } else {
            return null;
        }

        $ad_meta = SSSP_Main::get_ad_meta($post_id);
        $verify = $this->verify_ad_post($post_id, $ad_meta);

        if (!$verify) {
            return null;
        }

        $html = '';

        switch ($ad_meta['sssp-zone-position']) {
            case 'popup':
                if (!current_user_can(SEZNAM_SSP_CAPABILITY)) {
                    $html .= '<div class="sssp-deprecated-ads"><h5>' . esc_attr__('Plugin Seznam SSP', SEZNAM_SSP_SLUG) . '</h5>' . esc_attr__('This type of ad cannot be displayed in the shortcode', SEZNAM_SSP_SLUG) . '</div>';
                }
                break;
            case 'headerb':
                $html .= $this->render_branding($post_id, $ad_meta);
                break;
            case 'widget':
                $height = ', "height": ' . $ad_meta['sssp-zone-height'];
                $class = $this->get_class($ad_meta);
                $style = $this->get_style($ad_meta);

                $zone_id = 'sssp_ad_' . $ad_meta['sssp-zone-id'] . '_' . rand(0, 889) . '';

                $html .= '<div id="sssp_ad_' . $post_id . '_' . rand(0, 889) . '" ' . $class . $style . ' data-ssp-ad-object=\'{"zoneId": ' . $ad_meta['sssp-zone-id'] . ',"id": "' . $zone_id . '", "width": ' . $ad_meta['sssp-zone-width'] . $height . '}\' data-ssp-ad-id=\'' . $zone_id . '\'>';
                $html .= '<div id="' . $zone_id . '" data-szn-ssp-ad=\'{"zoneId": ' . $ad_meta['sssp-zone-id'] . ', "width": ' . $ad_meta['sssp-zone-width'] . $height . '}\'></div>';
                $html .= '<div class="sssp-clear"></div>';
                $html .= $this->get_dev_mode_bar($ad_meta);
                $html .= '</div>';
                break;
            default:
                $height = ', "height": ' . $ad_meta['sssp-zone-height'];
                $class = self::get_class($ad_meta);
                $style = self::get_style($ad_meta);

                $html .= '<div id="sssp_ad_' . $post_id . '_' . rand(0, 889) . '" ' . $class . $style . '>';
                $html .= '<div id="sssp_ad_' . $ad_meta['sssp-zone-id'] . '_' . rand(0, 889) . '" data-szn-ssp-ad=\'{"zoneId": ' . $ad_meta['sssp-zone-id'] . ', "width": ' . $ad_meta['sssp-zone-width'] . $height . '}\'></div>';
                $html .= '<div class="sssp-clear"></div>';
                $html .= $this->get_dev_mode_bar($ad_meta);
                $html .= '</div>';
                break;
        }

        return $html;
    }

    /**
     * @param $post_id
     * @param $ad_meta
     * @return bool
     */
    public function verify_ad_post($post_id, $ad_meta)
    {

        if (get_post_type($post_id) != SEZNAM_SSP_SLUG) {
            return false;
        }

        //Check newsfeed option
        if (isset($ad_meta['sssp-ad-newsfeed'])) {

            //Check, the NewsFeed Cookie
            $sssp_newsfeed = SSSP_Main::check_ssp_newsfeed();

            if ($ad_meta['sssp-ad-newsfeed'] != $sssp_newsfeed) {
                return false;
            }
        }

        //Check if the plugin is active
        if (!isset($ad_meta['sssp-ad-active']) || $ad_meta['sssp-ad-active'] == 'notactive' || $ad_meta['sssp-ad-active'] == 'wait') {
            return false;
        }

        //Check if the plugin is in development mode and if yes and user does not have an access return false
        //Indev should be filtered even before this verification. But for a security reasons and because it is not take any performance let it be here.
        if ($ad_meta['sssp-ad-active'] == 'indev' && !current_user_can(SEZNAM_SSP_CAPABILITY)) {
            return false;
        }

        if ($ad_meta['sssp-zone-insert'] == 'automatic' && $ad_meta['sssp-zone-position'] == 'inarticle' ||
            $ad_meta['sssp-zone-position'] == 'below_a') {
            global $post;
            if ($post) {
                $allowed_posts_array = unserialize(get_post_meta($post_id, 'sssp-ad-allowed-posts', true));
                if (!in_array($post->post_type, $allowed_posts_array)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param $ad_meta
     * @return string
     */
    public static function get_class($ad_meta)
    {
        $class = 'sssp-seznam-ad';
        if (isset($ad_meta['sssp-ad-custom-css'])) {
            $class = $class . ' ' . $ad_meta['sssp-ad-custom-css'];
        }
        if (isset($ad_meta['sssp-ad-alignment'])) {
            switch ($ad_meta['sssp-ad-alignment']) {
                case 'center':
                    $class = $class . ' sssp-seznam-ad-center';
                    break;
                case 'left':
                    $class = $class . ' sssp-seznam-ad-left';
                    break;
                case 'right':
                    $class = $class . ' sssp-seznam-ad-right';
                    break;
                case 'none':
                    $class = $class . ' sssp-seznam-ad-none';
                    break;
            }
        }

        if (isset($ad_meta['sssp-ad-type'])) {
            switch ($ad_meta['sssp-ad-type']) {
                case 'mobile':
                    $class = $class . ' sssp-seznam-ad-mobile';
                    break;
                case 'desktop':
                    $class = $class . ' sssp-seznam-ad-desktop';
                    break;
            }
        }

        return 'class="' . $class . '"';
    }

    /**
     * @param $ad_meta
     * @return string|void
     */
    public static function get_style($ad_meta)
    {

        if (isset($ad_meta['sssp-ad-margin-top']) && isset($ad_meta['sssp-ad-margin-bottom'])) {
            $margin_top = $ad_meta['sssp-ad-margin-top'];
            $margin_bottom = $ad_meta['sssp-ad-margin-bottom'];

            switch (true) {
                case $margin_top < 0:
                case $margin_top > 0:
                    $margin_top_value = 'margin-top: ' . $margin_top . 'px;';
                    break;
                default:
                    $margin_top_value = false;
                    break;
            }

            switch (true) {
                case $margin_bottom < 0:
                case $margin_bottom > 0:
                    $margin_bottom_value = 'margin-bottom: ' . $margin_bottom . 'px;';
                    break;
                default:
                    $margin_bottom_value = false;
                    break;
            }

            switch (true) {
                case $margin_top_value && $margin_bottom_value:
                    $style = ' style="' . $margin_top_value . ' ' . $margin_bottom_value . '"';
                    break;
                case $margin_top_value && !$margin_bottom_value:
                    $style = ' style="' . $margin_top_value . '"';
                    break;
                case !$margin_top_value && $margin_bottom_value:
                    $style = ' style="' . $margin_bottom_value . '"';
                    break;
                default:
                    $style = '';
                    break;
            }
            return $style;
        }

    }

    /**
     *
     */
    public function js_to_header()
    {
        $header_code = get_option('sssp-mobile-breakpoint', '700');
        echo "<script>function sssp_get_breakpoint(){
        return '" . $header_code . "';
        }</script>";

    }

    /**
     * @param $post_id
     * @param $ad_meta
     * @return string
     */
    private function render_branding($post_id, $ad_meta)
    {

        $class = $this->get_class($ad_meta);
        $style = $this->get_style($ad_meta);

        $html = '<div id="sssp_ad_' . $post_id . '" ' . $class . $style . '>';
        $html .= '<div id="ssp-zone-header" class="sssp-desktop"></div>';
        $html .= '</div>';

        $html .= '<style>' . get_option('sssp-branding-css', $this->custom_css_branding()) . '</style>';

        $html .= '<script>';

        $html .= 'if (window.innerWidth >= 1366) {
                    document.body.insertAdjacentHTML("afterbegin", "<div id=\"ssp-zone-header-branding\"></div>");
                    var adZone = {
                            zoneId: ' . $ad_meta['sssp-zone-id'] . ',	// číselné ID zóny
                        width: 2000,	// maximální šířka požadované reklamy
                        id: "ssp-zone-header",	// id elementu pro leaderboard
                        elements: [
                            {id: "ssp-zone-header", width: 970},	// stačí uvést šířku
                            {id: "ssp-zone-header-branding", width: 2000, height: 1400}	// je nutné uvést šířku i výšku
                        ]
                    };
                } else {
                    var adZone = {
                        zoneId: ' . $ad_meta['sssp-zone-id'] . ',
                        width: ' . $ad_meta['sssp-zone-width'] . ',
                        height: ' . $ad_meta['sssp-zone-height'] . ',
                        id: "ssp-zone-header"
                    }
                };
                sssp.getAds(adZone);
                ';
        $html .= '</script>';

        return $html;
    }

    /**
     * @return string
     */
    private function custom_css_branding()
    {

        require_once(SEZNAM_SSP_PATH . 'includes/custom-post/class-custom-post-admin.php');
        return SSSP_CustomPostAdmin::custom_css_branding();
    }

    /**
     * @return string|void
     */
    private function get_dev_mode_bar($ad_meta)
    {
        if (isset($_GET['sssp-dev']) && $_GET['sssp-dev'] == 0) {
            return;
        }

        if (isset($_COOKIE['sssp-dev-mode']) && $_COOKIE['sssp-dev-mode'] != '0' || isset($_GET['sssp-dev']) && $_GET['sssp-dev'] != '0') {
            $html = '<div class="sssp-dev-mode">';
            $html .= 'Nm: <strong>' . $ad_meta['post_title'] . '</strong><br> ZID: <strong>' . $ad_meta['sssp-zone-id'] . '</strong>; WxH: <strong>' . $ad_meta['sssp-zone-width'] . 'x' . $ad_meta['sssp-zone-height'] . '</strong>; In: <strong>' . $ad_meta['sssp-zone-insert'] . '</strong>; Pos: <strong>' . $ad_meta['sssp-zone-position'] . '</strong>';
            if (isset($_COOKIE['sssp-dev-mode']) && $_COOKIE['sssp-dev-mode'] == md5('sssp-dev-hash') || isset($_GET['sssp-dev-hash']) && $_GET['sssp-dev-hash'] == md5('sssp-dev-hash')) {
                $html .= '<br><span id="sssp-reload-ads">' . esc_attr__('Reload Ads', SEZNAM_SSP_SLUG) . '</span>';
            }
            if (is_user_logged_in() && current_user_can(SEZNAM_SSP_CAPABILITY)) {
                $html .= '<br><span id="sssp-edit-ads"><a target="_blank" href="' . SEZNAM_SSP_ADMIN_URL . 'post.php?post=' . $ad_meta['post_ID'] . '&action=edit">' . esc_attr__('Edit Ad', SEZNAM_SSP_SLUG) . '</a></span>';
            }
            $html .= '</div>';
            return $html;
        }
    }
}