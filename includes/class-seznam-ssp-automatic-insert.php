<?php

if (!defined('WPINC')) {
    wp_die();
}

class SSSP_AutomaticInsert
{
    protected static $instance = NULL;

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    public function __construct()
    {
        //Actions
        add_action('get_footer', [$this, 'ads_to_footer']);
        add_action('wp_body_open', [$this, 'ads_to_header']);
        add_action('wp_body_open', [$this, 'ads_to_header_with_branding']);

        //Filters
        add_filter('the_content', [$this, 'prefix_insert_post_ads'], 8);
        add_filter('the_content', [$this, 'ads_below_article'], 9);
        add_filter('the_content', [$this, 'add_popup_div'], 10);

    }

    /**
     * Return an instance of this class.
     *
     * @return    object    A single instance of this class.
     * @since     1.0.0
     *
     */
    public static function get_instance()
    {
        // If the single instance hasn't been set, set it now.
        if (NULL == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     *
     */
    public function ads_to_footer()
    {
        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key' => 'sssp-zone-position',
            'meta_value' => 'footer',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'relation' => 'OR',
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'active',
                        'compare' => '=',
                    ],
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'indev',
                        'compare' => '=',
                    ],
                ],
                [
                    'key' => 'sssp-zone-insert',
                    'value' => 'automatic',
                ],
            ],
        ];
        echo self::render_ads($args);
    }

    /**
     *
     */
    public function ads_to_header()
    {
        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key' => 'sssp-zone-position',
            'meta_value' => 'header',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'relation' => 'OR',
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'active',
                        'compare' => '=',
                    ],
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'indev',
                        'compare' => '=',
                    ],
                ],
                [
                    'key' => 'sssp-zone-insert',
                    'value' => 'automatic',
                ],
            ],
        ];
        echo self::render_ads($args);
    }

    /**
     *
     */
    public function ads_to_header_with_branding()
    {
        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => 1,
            'post_status' => 'publish',
            'meta_key' => 'sssp-zone-position',
            'meta_value' => 'headerb',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'relation' => 'OR',
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'active',
                        'compare' => '=',
                    ],
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'indev',
                        'compare' => '=',
                    ],
                ],
                [
                    'key' => 'sssp-zone-insert',
                    'value' => 'automatic',
                ],
            ],
        ];
        echo self::render_ads($args);
    }

    /**
     * @param $content
     * @return string
     */
    public function ads_below_article($content)
    {
        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key' => 'sssp-zone-position',
            'meta_value' => 'below_a',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'relation' => 'OR',
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'active',
                        'compare' => '=',
                    ],
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'indev',
                        'compare' => '=',
                    ],
                ],
                [
                    'key' => 'sssp-zone-insert',
                    'value' => 'automatic',
                ],
            ],
        ];
        $ads = self::render_ads($args);

        $sticky_end = '<div id="sssp-content-end"></div>';

        return $content . $ads . $sticky_end;

    }

    /**
     * @param $content
     * @return mixed|string|void
     */
    public function add_popup_div($content)
    {
        //Check, the NewsFeed Cookie
        $sssp_newsfeed = SSSP_Main::check_ssp_newsfeed();

        $mz_settings = get_option('sssp-media-contract', '0');

        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key' => 'sssp-zone-position',
            'meta_value' => 'popup',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'relation' => 'OR',
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'active',
                        'compare' => '=',
                    ],
                    [
                        'key' => 'sssp-ad-active',
                        'value' => 'indev',
                        'compare' => '=',
                    ],
                ],
                [
                    'key' => 'sssp-ad-newsfeed',
                    'value' => $sssp_newsfeed,
                    'compare' => '=',
                ],

            ],
        ];
        $popups = get_posts($args);
        if ($popups) {
            $html = null;
            $zoneID_mobile = null;
            $zoneID_desktop = null;
            foreach ($popups as $popup) {
                $ad_meta = SSSP_Main::get_ad_meta($popup->ID);

                if ($ad_meta['sssp-ad-type'] == 'mobile') {
                    $zoneID_mobile = 'data-zoneidm="' . $ad_meta['sssp-zone-id'] . '"';
                } else {
                    $zoneID_desktop = 'data-zoneidd="' . $ad_meta['sssp-zone-id'] . '"';
                }

            }

            if ($this->check_popup($ad_meta)) {
                $html .= '<style>' . get_option('sssp-popup-css', $this->custom_css_popup()) . '</style>';
                $popup_dov = '<div id="ssspShowPopUp"
                            ' . $zoneID_mobile . '
                            ' . $zoneID_desktop . '
                            data-width="' . get_option('sssp-mobile-breakpoint', '700') . '"
                            data-mz="' . $mz_settings . '"
                            data-nf="' . $sssp_newsfeed . '"
                            >                     
                            </div>';

                $html .= $popup_dov;
            }

            return $content . $html;
        }

        return $content;
    }

    /**
     * @param $ad_meta
     * @return bool
     */
    private function check_popup($ad_meta)
    {

        if (isset($ad_meta['sssp-ad-active']) && ($ad_meta['sssp-ad-active'] == 'wait' || $ad_meta['sssp-ad-active'] == 'notactive')) {
            return false;
        }
        if (isset($ad_meta['sssp-ad-active']) && $ad_meta['sssp-ad-active'] == 'indev' && !current_user_can(SEZNAM_SSP_CAPABILITY)) {
            return false;
        }

        global $post;
        if ($post) {
            $allowed_posts_array = unserialize(get_post_meta($ad_meta['post_ID'], 'sssp-ad-allowed-posts', true));
            if (!in_array($post->post_type, $allowed_posts_array)) {
                return false;
            }
        }

        return true;

    }

    /**
     * @param $content
     * @return string|void
     */
    public function prefix_insert_post_ads($content)
    {
        $args = [
            'post_type' => SEZNAM_SSP_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key' => 'sssp-zone-position',
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key' => 'sssp-ad-active',
                    'value' => 'active',
                    'compare' => '=',
                ],
                [
                    'key' => 'sssp-ad-active',
                    'value' => 'indev',
                    'compare' => '=',
                ],
            ],
            'meta_value' => 'inarticle',
        ];
        return self::render_inarticle_ads($args, $content);
    }

    /**
     * @param $insertion
     * @param $paragraph_id
     * @param $content
     * @param $repeat
     * @return string
     */
    public function prefix_insert_after_paragraph($insertion, $paragraph_id, $content, $repeat)
    {

        $content = str_replace("<p></p>", "", $content);
        $closing_p = '</p>';
        $paragraphs = explode($closing_p, $content);
        $paragraphs = array_filter($paragraphs); //remove empty '' strings

        $paragraph_id_orig = $paragraph_id;

        $paragraph_repeat_start = $paragraph_id + 3;

        if($this->check_the_last_paragraph($paragraphs)){
            $paragraphs_count_total = count($paragraphs);
        }else{
            $paragraphs_count_total = count($paragraphs)-1;
        }

        $paragraphs_count = $paragraphs_count_total - $paragraph_id;

        if ($paragraph_id + 2 < $paragraphs_count_total) {
            foreach ($paragraphs as $index => $paragraph) {


                if (trim($paragraph)) {
                    $paragraphs[$index] .= $closing_p;
                }

                if ($paragraph_id == $index + 1) {
                    $paragraphs[$index] .= $insertion;
                }

                if ('yes' == $repeat && $paragraphs_count >= 7) {
                    if ($paragraph_repeat_start == $index + 3 - $paragraph_id) {
                        $paragraphs[$paragraph_repeat_start] .= $insertion;
                        $paragraph_repeat_start = $paragraph_repeat_start + 4;
                        $paragraphs_count = $paragraphs_count - 4;
                    }
                }
            }
        }else{
            return $content;
        }


        $paragraphs = implode('', $paragraphs);


        if(is_numeric($paragraph_id_orig) && empty($paragraph_id_orig)){
            $paragraphs = $insertion.$paragraphs;
        }

        return $paragraphs;

    }

    /**
     * @param $args
     * @return string
     */
    private function render_ads($args)
    {
        $ads = get_posts($args);

        $html = '';
        foreach ($ads as $ad) {
            $html .= do_shortcode('[seznam-ads id="' . $ad->ID . '"]');
        }
        return $html;
    }


    /**
     * aram $args
     * @return string|void
     */
    private function render_inarticle_ads($args, $content)
    {
        $ads = get_posts($args);

        if(!$ads){
            return $content;
        }

        foreach ($ads as $ad) {
            $ad_meta = SSSP_Main::get_ad_meta($ad->ID);

            if(!isset($ad_meta['sssp-zone-inarticle-placement'])){
                $ad_meta['sssp-zone-inarticle-placement'] = get_post_meta($ad->ID, 'sssp-zone-inarticle-placement', true);
            }
            if(!isset($ad_meta['sssp-zone-inarticle-placement-repeat'])){
                $ad_meta['sssp-zone-inarticle-placement-repeat'] = get_post_meta($ad->ID, 'sssp-zone-inarticle-placement-repeat', true);
            }

            if (
                $ad_meta['sssp-ad-active'] == 'active' ||
                $ad_meta['sssp-ad-active'] == 'indev' && current_user_can(SEZNAM_SSP_CAPABILITY)
            ) {
                $content = self::prefix_insert_after_paragraph('[seznam-ads id="' . $ad->ID . '"]', $ad_meta['sssp-zone-inarticle-placement'], $content, $ad_meta['sssp-zone-inarticle-placement-repeat']);
            }

        }
        return $content;
    }


    /**
     * @return string
     */
    private function custom_css_popup()
    {
        require_once(SEZNAM_SSP_PATH . 'includes/custom-post/class-custom-post-admin.php');
        return SSSP_CustomPostAdmin::custom_css_popup();
    }

    /**
     * @param $paragraphs
     * @return bool
     */
    private function check_the_last_paragraph($paragraphs){
        $last_paragraph = end($paragraphs);
        $pos = strpos($last_paragraph, '</p>');

        if($pos === false){
            return false;
        }

        return true;
    }

}